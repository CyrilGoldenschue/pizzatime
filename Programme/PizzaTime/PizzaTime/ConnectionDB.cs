﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace PizzaTime
{
    /// <summary>
    /// contains methods and attributes to connect and deal with the database
    /// </summary>
    class ConnectionDB
    {
        //connection to the database
        private SQLiteConnection m_dbConnection;

        /// <summary>
        /// constructor : creates the connection to the database SQLite
        /// </summary>
        public ConnectionDB()
        {
            if (File.Exists("PizzaTime.sqlite"))
            {
                m_dbConnection = new SQLiteConnection("Data Source=PizzaTime.sqlite;Version=3;");
                m_dbConnection.Open();
            }
            else
            {
                SQLiteConnection.CreateFile("PizzaTime.sqlite");
                m_dbConnection = new SQLiteConnection("Data Source=PizzaTime.sqlite;Version=3;");
                m_dbConnection.Open();
                //create the table Customers
                CreateTableCustomers();
                //create the table Factures
                CreateTableFactures();
                //create the table TypeProducts
                CreateTableTypeProducts();
                //create the table Products
                CreateTableProducts();
                //create the table ProductsHasFactures
                CreateTableProductsHasFactures();
            }
        }

        #region CreateTable

        /// <summary>
        /// create the "Customers" table and insert data
        /// </summary>
        private void CreateTableCustomers()
        {
            string sql = "CREATE TABLE Customers (idCustomer INT PRIMARY KEY NOT NULL," +
                " NameCustomer VARCHAR(225)," +
                " FirstNameCustomer VARCHAR(225)," +
                " RueCustomer VARCHAR(225)," +
                " NumRueCustomer INT," +
                " NPACustomer INT," +
                " EmailCustomer Text," +
                " TelCustomer Text," +
                " VilleCustomer VARCHAR(128))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// create the "Factures" table and insert data
        /// </summary>
        private void CreateTableFactures()
        {
            string sql = "CREATE TABLE Factures (idFacture INT PRIMARY KEY NOT NULL," +
                " DateFacture TEXT," +
                " FkCustomer INT," +
                " ExportFacture INT," +
                " PriceTotalFacture INT," +
                " ExportDate TEXT," +
                " TauxTVA INT," +
                "FOREIGN KEY (FkCustomer) REFERENCES Customers(idCustomer))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// create the "TypeProducts" table and insert data
        /// </summary>
        private void CreateTableTypeProducts()
        {
            string sql = "CREATE TABLE TypeProducts (idTypeProduct INT PRIMARY KEY NOT NULL," +
                " NameTypeProduct VARCHAR(64))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "insert into TypeProducts(idTypeProduct, NameTypeProduct) values(1, 'Pizza')"; command = new SQLiteCommand(sql, m_dbConnection); command.ExecuteNonQuery();
            sql = "insert into TypeProducts(idTypeProduct, NameTypeProduct) values(2, 'Dessert')"; command = new SQLiteCommand(sql, m_dbConnection); command.ExecuteNonQuery();
            sql = "insert into TypeProducts(idTypeProduct, NameTypeProduct) values(3, 'Boisson')"; command = new SQLiteCommand(sql, m_dbConnection); command.ExecuteNonQuery();
        }

        /// <summary>
        /// create the "Products" table and insert data
        /// </summary>
        private void CreateTableProducts()
        {
            string sql = "CREATE TABLE Products (idProduct INT PRIMARY KEY NOT NULL," +
                " LabelProduct VARCHAR(64)," +
                " PriceProduct INT," +
                " QteProduct INT," +
                " FkTypeProduct INT," +
                "FOREIGN KEY (FkTypeProduct) REFERENCES TypeProducts(idTypeProduct))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "insert into Products(idProduct, LabelProduct, PriceProduct, QteProduct, FkTypeProduct) values(1, 'Marguerita', 13, 9, 1)"; command = new SQLiteCommand(sql, m_dbConnection); command.ExecuteNonQuery();

        } 

        /// <summary>
        /// create the "ProductsHasFactures" table and insert data
        /// </summary>
        private void CreateTableProductsHasFactures()
        {
            string sql = "CREATE TABLE ProductsHasFactures (" +
                " FkProduct INT NOT NULL," +
                " FkFacture INT NOT NULL," +
                " QteProductFacture INT," +
                " PriceTotalProduct INT," +
                " FOREIGN KEY (FkProduct) REFERENCES Products(idProduct)," +
                " FOREIGN KEY (FkFacture) REFERENCES Factures(idFacture))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        #endregion CreateTable

        #region Product

        /// <summary>
        /// Get any informations of the product
        /// </summary>
        /// <returns></returns>
        public List<string> GetProduct()
        {
            List<String> InfoProduct = new List<string>();
            string sql = "SELECT LabelProduct, PriceProduct, QteProduct, NameTypeProduct FROM Products JOIN TypeProducts ON Products.FkTypeProduct = TypeProducts.idTypeProduct";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                InfoProduct.Add(reader["LabelProduct"].ToString());
                InfoProduct.Add((Convert.ToDouble(reader["PriceProduct"].ToString())/100).ToString());
                InfoProduct.Add(reader["QteProduct"].ToString());
                InfoProduct.Add(reader["NameTypeProduct"].ToString());

            }

            return InfoProduct;
        }

        /// <summary>
        /// Get the id of one product in relation to the product name
        /// </summary>
        /// <param name="label">the name of the product</param>
        /// <returns></returns>
        public int GetIdProduct(string label)
        {
            label= label.Replace("'", " ");
            int id = 0;
            string sql = "SELECT idProduct FROM Products WHERE LabelProduct = '" + label + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                id = Convert.ToInt32(reader["idProduct"]);
            }

            return id;
        }

        /// <summary>
        /// Update the information of a Product
        /// </summary>
        /// <param name="idProduct">identity of product</param>
        /// <param name="label">label of product</param>
        /// <param name="price">price of product</param>
        /// <param name="qte">quantity of product</param>
        /// <param name="typeProduct"> type of the product</param>
        /// <returns></returns>
        public void UpdateProduct(int idProduct, string label, int price, int qte, string typeProduct)
        {
            label= label.Replace("'", " ");
            int ID = 0;
            string sqlP = "SELECT idTypeProduct, NameTypeProduct FROM TypeProducts";
            SQLiteCommand commandP = new SQLiteCommand(sqlP, m_dbConnection);
            SQLiteDataReader reader = commandP.ExecuteReader(); ;
            while (reader.Read())
            {
                if(reader["NameTypeProduct"].ToString() == typeProduct){

                    ID = Convert.ToInt32(reader["idTypeProduct"]);

                }

            }
            string sql = "UPDATE Products SET LabelProduct = '" + label + "', PriceProduct = " + price + ", QteProduct = " + qte + ", FKTypeProduct = " + ID + " WHERE idProduct = " + idProduct;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Delete a quantity of a product for a command
        /// </summary>
        /// <param name="idProduct">identity of product</param>
        /// <param name="qteDelete">quantity of product ot delete</param>
        /// <returns></returns>
        public void DeleteQtyProduct(int idProduct, int qteDelete)
        {

            int Quantity = 0;
            string sql = "SELECT QteProduct FROM Products WHERE idProduct = " + idProduct;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                Quantity = Convert.ToInt32(reader["QteProduct"]);
            }
            qteDelete = Quantity - qteDelete;

            sql = "UPDATE Products SET QteProduct = " + qteDelete + " WHERE idProduct = " + idProduct;
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }


        /// <summary>
        /// Add a Product
        /// </summary>
        /// <param name="name">name of the customer</param>
        /// <param name="firstname">firstname of the customer</param>
        /// <param name="address">address of the customer</param>
        /// <param name="nbAddress">number of address of the customer</param>
        /// <param name="city">city of the customer</param>
        /// <param name="npa">npa of the customer</param>
        /// <returns></returns>
        public void CreateProduct(string label, int price, int qte, string typeProduct)
        {
            label = label.Replace("'", " ");
            label = label.Replace(" ", "-");
            int idTypeProduct = 0;
            string sqlP = "SELECT idTypeProduct, NameTypeProduct FROM TypeProducts";
            SQLiteCommand commandP = new SQLiteCommand(sqlP, m_dbConnection);
            SQLiteDataReader reader = commandP.ExecuteReader(); ;
            while (reader.Read())
            {
                if (reader["NameTypeProduct"].ToString() == typeProduct)
                {

                    idTypeProduct = Convert.ToInt32(reader["idTypeProduct"]);

                }

            }

            int idProduct = 0;
            string sqlid = "SELECT idProduct FROM Products";
            SQLiteCommand commandid = new SQLiteCommand(sqlid, m_dbConnection);
            SQLiteDataReader readerid = commandid.ExecuteReader(); ;
            while (readerid.Read())
            {
                idProduct = Convert.ToInt32(readerid["idProduct"]);
            }
            idProduct++;


            string sql = "INSERT INTO Products VALUES (" + idProduct + ",'" + label + "', " + price + ", " + qte + ", " + idTypeProduct + ")";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// delete a Product
        /// </summary>
        /// <param name="label">name of the customer</param>
        /// <returns></returns>
        public void DeleteProduct(string label)
        {
            label= label.Replace("'", " ");
            string sql = "DELETE FROM Products WHERE LabelProduct = '" + label + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Add quantity in a Product
        /// </summary>
        /// <param name="labelProduct">name of the product</param>
        /// <param name="qty">quantity of product</param>
        /// <returns></returns>
        public void addProductQte(string labelProduct, int qty)
        {
            labelProduct= labelProduct.Replace("'", " ");
            int Quantity = 0;
            string sqlP = "SELECT QteProduct FROM Products WHERE LabelProduct = '" + labelProduct + "'";
            SQLiteCommand commandP = new SQLiteCommand(sqlP, m_dbConnection);
            SQLiteDataReader reader = commandP.ExecuteReader(); ;
            while (reader.Read())
            {

                    Quantity = Convert.ToInt32(reader["QteProduct"]);

            }

            Quantity += qty;

            string sql = "UPDATE Products SET QteProduct = " + Quantity + " WHERE LabelProduct = '" + labelProduct + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// get type of the product
        /// </summary>
        /// <returns></returns>
        public List<string> GetTypeProduct()
        {
            List<string> TypeProduct = new List<string>();
            string sql = "SELECT NameTypeProduct FROM TypeProducts";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                TypeProduct.Add(reader["NameTypeProduct"].ToString());
            }

            return TypeProduct;
        }

        #endregion Product

        #region Facture

        /// <summary>
        /// Create a facture
        /// </summary>
        /// <param name="nameCustomer">name of the customer</param>
        /// <param name="totalprice">total price of facture</param>
        /// <param name="TVA">Taxe TVA for the price</param>
        /// <returns></returns>
        public int CreateFacture(string nameCustomer, double totalprice, int TVA)
        {
            nameCustomer= nameCustomer.Replace("'", " ");
            int idCustomer = 0;
            string sqlidCustomer = "SELECT idCustomer FROM Customers WHERE NameCustomer = '" + nameCustomer + "'";
            SQLiteCommand commandidCustomer = new SQLiteCommand(sqlidCustomer, m_dbConnection);
            SQLiteDataReader readeridCustomer = commandidCustomer.ExecuteReader(); ;
            while (readeridCustomer.Read())
            {
                idCustomer = Convert.ToInt32(readeridCustomer["idCustomer"]);
            }

            int idFacture = 0;
            string sqlidFacture = "SELECT idFacture FROM Factures";
            SQLiteCommand commandidFacture = new SQLiteCommand(sqlidFacture, m_dbConnection);
            SQLiteDataReader readeridFacture = commandidFacture.ExecuteReader(); ;
            while (readeridFacture.Read())
            {
                idFacture = Convert.ToInt32(readeridFacture["idFacture"]);
            }
            idFacture++;

            string DateFacture = DateTime.Today.Day + "." + DateTime.Today.Month + "." + DateTime.Today.Year.ToString().Substring(2);

            //insert value in Factures table
            string sql = "INSERT INTO Factures VALUES (" + idFacture + ", '" + DateFacture + "', " + idCustomer + ", 0, " + totalprice*100 + ", '', " + TVA + ")";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            return idFacture;

        }

        public void CreateProductHasFactures(int idFacture, string products, int qte, double totalPrice, bool Add)
        {
            products= products.Replace("'", " ");
            int idProduct = 0;
            string sqlidProduct = "SELECT idProduct FROM Products WHERE LabelProduct = '" + products + "'";
            SQLiteCommand commandidProduct = new SQLiteCommand(sqlidProduct, m_dbConnection);
            SQLiteDataReader readeridProduct = commandidProduct.ExecuteReader(); ;
            while (readeridProduct.Read())
            {
                idProduct = Convert.ToInt32(readeridProduct["idProduct"]);
            }

            //insert value in ProductsHasFactures table
            string sqlPF = "INSERT INTO ProductsHasFactures VALUES (" + idProduct + ", " + idFacture + ", " + qte + ", " + totalPrice*100 + ")";
            SQLiteCommand commandPF = new SQLiteCommand(sqlPF, m_dbConnection);
            commandPF.ExecuteNonQuery();

            if (Add)
            {
                double price = 0;
                string sqlPrice = "SELECT PriceTotalFacture FROM Factures WHERE idFacture = " + idFacture;
                SQLiteCommand commandPrice = new SQLiteCommand(sqlPrice, m_dbConnection);
                SQLiteDataReader readerPrice = commandPrice.ExecuteReader(); ;
                while (readerPrice.Read())
                {
                    price = Convert.ToDouble(readerPrice["PriceTotalFacture"])/100;
                }
                price += totalPrice;

                string sqlPriceF = "UPDATE Factures SET PriceTotalFacture = " + price*100 + " WHERE idFacture = " + idFacture;
                SQLiteCommand commandPriceF = new SQLiteCommand(sqlPriceF, m_dbConnection);
                commandPriceF.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Get id of facture
        /// </summary>
        /// <param name="date">date of facture search</param>
        /// <param name="name">name of customer link with the facture search</param>
        /// <param name="firstname">firstname of customer link with the facture search</param>
        /// <returns></returns>
        public int GetIdFacture(string date, string name, string firstname, int Prix)
        {
            name= name.Replace("'", " ");
            firstname = firstname.Replace("'", " ");
            int IdFacture = 0;
            string sql = "SELECT idFacture FROM Factures INNER JOIN Customers ON Factures.FkCustomer = Customers.idCustomer WHERE DateFacture = '" + date + "' AND NameCustomer = '" + name + "' AND FirstNameCustomer = '" + firstname + "' AND PriceTotalFacture = " + Prix;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                IdFacture = Convert.ToInt32(reader["idFacture"]);
            }

            return IdFacture;
        }

        /// <summary>
        /// Get TVA for a facture
        /// </summary>
        /// <param name="idFacture">the id of the facture search</param>
        /// <returns></returns>
        public int GetTVAFacture(int idFacture)
        {
            int TVA = 0;
            string sql = "SELECT TauxTVA FROM Factures WHERE idFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                TVA = Convert.ToInt32(reader["TauxTVA"]);
            }

            return TVA;
        }

        /// <summary>
        /// Update the information of a facture
        /// </summary>
        /// <param name="name">Name of the customer</param>
        /// <param name="firstName">Firstname of the customer</param>
        /// <param name="qte">Quantity of product</param>
        /// <param name="totalPrice">Total of price for the facture</param>
        /// <param name="date"> Date of the facture</param>
        /// <returns></returns>
        public void UpdateFacture(int idFacture, string name, string firstName, double totalPrice, int TVA)
        {
            name= name.Replace("'", " ");
            firstName= firstName.Replace("'", " ");
            int idCustomer = 0;
            string sqlCustomer = "SELECT idCustomer FROM Customers WHERE NameCustomer = '" + name + "' AND FirstNameCustomer = '" + firstName + "'";
            SQLiteCommand commandCustomer = new SQLiteCommand(sqlCustomer, m_dbConnection);
            SQLiteDataReader readerCustomer = commandCustomer.ExecuteReader();
            while (readerCustomer.Read())
            {
                idCustomer = Convert.ToInt32(readerCustomer["idCustomer"]);
            }

            string sql = "UPDATE Factures SET FkCustomer = " + idCustomer + ", PriceTotalFacture = " + totalPrice*100 + ", TauxTVA = " + TVA + " WHERE idFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();


        }

        /// <summary>
        /// Update the information of a Product link with a facture
        /// </summary>
        /// <param name="idFacture">identity of facture</param>
        /// <param name="product">Name of product to update</param>
        /// <param name="qteAdd">Quantity to add</param>
        /// <param name="totalPriceAdd">Price to add</param>
        /// <returns></returns>
        public void UpdateProductHasFacture(int idFacture, string product, int qteAdd, double totalPriceAdd)
        {
            product.Replace("'", " ");
            int idProduct = 0;
            string sqlProduct = "SELECT idProduct FROM Products WHERE LabelProduct = '" + product + "'";
            SQLiteCommand commandProduct = new SQLiteCommand(sqlProduct, m_dbConnection);
            SQLiteDataReader readerProduct = commandProduct.ExecuteReader();
            while (readerProduct.Read())
            {
                idProduct = Convert.ToInt32(readerProduct["idProduct"]);
            }

            int QteBase = 0;
            double TotalPriceBase = 0;
            string sqlProductInfo = "SELECT QteProductFacture, PriceTotalProduct FROM ProductsHasFactures WHERE FkProduct = " + idProduct + " AND FkFacture = " + idFacture;
            SQLiteCommand commandProductInfo = new SQLiteCommand(sqlProductInfo, m_dbConnection);
            SQLiteDataReader readerProductInfo = commandProductInfo.ExecuteReader();
            while (readerProductInfo.Read())
            {
                QteBase = Convert.ToInt32(readerProductInfo["QteProductFacture"]);
                TotalPriceBase = Convert.ToDouble(readerProductInfo["PriceTotalProduct"])/100;
            }
            QteBase += qteAdd;
            TotalPriceBase += totalPriceAdd;

            string sqlProductF = "UPDATE ProductsHasFactures SET QteProductFacture = " + QteBase + ", PriceTotalProduct = " + TotalPriceBase*100 + "  WHERE FkFacture = " + idFacture + " AND FkProduct = " + idProduct;
            SQLiteCommand commandProductF = new SQLiteCommand(sqlProductF, m_dbConnection);
            commandProductF.ExecuteNonQuery();


            double price = 0;
            string sqlPrice = "SELECT PriceTotalFacture FROM Factures WHERE idFacture = " + idFacture;
            SQLiteCommand commandPrice = new SQLiteCommand(sqlPrice, m_dbConnection);
            SQLiteDataReader readerPrice = commandPrice.ExecuteReader(); ;
            while (readerPrice.Read())
            {
                price = Convert.ToDouble(readerPrice["PriceTotalFacture"])/100;
            }
            price += totalPriceAdd;

            string sqlPriceF = "UPDATE Factures SET PriceTotalFacture = " + price*100 + " WHERE idFacture = " + idFacture;
            SQLiteCommand commandPriceF = new SQLiteCommand(sqlPriceF, m_dbConnection);
            commandPriceF.ExecuteNonQuery();



        }

        /// <summary>
        /// Delete a product link with a facture
        /// </summary>
        /// <param name="idFacture">the id of facture to delete</param>
        /// <param name="product">the name of product to delete of a product link with a facture</param>
        /// <param name="priceProduct">the price of the product link with a facture</param>
        /// <returns></returns>
        public void DeleteProductHasFacture(int idFacture, string product, double priceProduct)
        {
            product.Replace("'", " ");
            int idProduct = 0;
            string sqlProduct = "SELECT idProduct FROM Products WHERE LabelProduct = '" + product + "'";
            SQLiteCommand commandProduct = new SQLiteCommand(sqlProduct, m_dbConnection);
            SQLiteDataReader readerProduct = commandProduct.ExecuteReader();
            while (readerProduct.Read())
            {
                idProduct = Convert.ToInt32(readerProduct["idProduct"]);
            }

            string sqlProductF = "DELETE FROM ProductsHasFactures WHERE FkProduct = " + idProduct + " AND FkFacture = " + idFacture;
            SQLiteCommand commandProductF = new SQLiteCommand(sqlProductF, m_dbConnection);
            commandProductF.ExecuteNonQuery();

            double price = 0;
            string sqlPrice = "SELECT PriceTotalFacture FROM Factures WHERE idFacture = " + idFacture;
            SQLiteCommand commandPrice = new SQLiteCommand(sqlPrice, m_dbConnection);
            SQLiteDataReader readerPrice = commandPrice.ExecuteReader();
            while (readerPrice.Read())
            {
                price = Convert.ToDouble(readerPrice["PriceTotalFacture"])/100;
            }
            price -= priceProduct;

            string sqlPriceF = "UPDATE Factures SET PriceTotalFacture = " + price*100 + " WHERE idFacture = " + idFacture;
            SQLiteCommand commandPriceF = new SQLiteCommand(sqlPriceF, m_dbConnection);
            commandPriceF.ExecuteNonQuery();

        }

        /// <summary>
        /// delete a Product
        /// </summary>
        /// <param name="priceFacture">Name of the product</param>
        /// <param name="idCustomer">Name of the product</param>
        /// <returns></returns>
        public void DeleteFacture(int idFacture)
        {
            string sql = "SELECT FkProduct, QteProductFacture FROM ProductsHasFactures WHERE FkFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                int Qty = 0;
                sql = "SELECT QteProduct FROM Products WHERE idProduct = " + reader["FkProduct"];
                command = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader readerQ = command.ExecuteReader();
                while (readerQ.Read())
                {
                    Qty = Convert.ToInt32(readerQ["QteProduct"].ToString());
                }
                Qty += Convert.ToInt32(reader["QteProductFacture"].ToString());
                sql = "UPDATE Products SET QteProduct = " + Qty + " WHERE idProduct = " + reader["FkProduct"];
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();
            }



            sql = "DELETE FROM ProductsHasFactures WHERE FkFacture = " + idFacture;
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "DELETE FROM Factures WHERE idFacture = " + idFacture;
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update the information of a facture
        /// </summary>
        /// <param name="name">Name of the customer</param>
        /// <param name="firstName">Firstname of the customer</param>
        /// <param name="priceFacture">Price of the facture selected</param>
        /// <returns></returns>
        public void ExportFacture(string name, string firstName, double priceFacture)
        {
            name= name.Replace("'", " ");
            firstName= firstName.Replace("'", " ");
            int idCustomer = 0;
            string sqlCustomer = "SELECT idCustomer FROM Customers WHERE NameCustomer = '" + name + "' AND FirstNameCustomer = '" + firstName + "'";
            SQLiteCommand commandCustomer = new SQLiteCommand(sqlCustomer, m_dbConnection);
            SQLiteDataReader readerCustomer = commandCustomer.ExecuteReader();
            while (readerCustomer.Read())
            {
                idCustomer = Convert.ToInt32(readerCustomer["idCustomer"]);
            }

            string DateFacture = DateTime.Today.Day + "." + DateTime.Today.Month + "." + DateTime.Today.AddYears(1).Year.ToString().Substring(2);

            string sql = "UPDATE Factures SET ExportFacture = 1, ExportDate = '" + DateFacture + "' WHERE PriceTotalFacture = " + priceFacture*100 + " AND FkCustomer = " + idCustomer;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update the TVA information of a facture
        /// </summary>
        /// <param name="idFacture">if of the facture</param>
        /// <param name="TVA">new TVA</param>
        /// <returns></returns>
        public void UpdateTVAFacture(int idFacture, int TVA)
        {
            string sql = "UPDATE Factures SET TauxTVA = "+TVA+" WHERE idFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Get any informations of facture
        /// </summary>
        /// <returns></returns>
        public List<string> GetFacture()
        {
            List<String> InfoFacture = new List<string>();
            int idProductHasFacture = 0;
            string sqlFacture = "SELECT idFacture, DateFacture, NameCustomer, PriceTotalFacture, ExportFacture, TauxTVA FROM Factures INNER JOIN Customers ON Customers.idCustomer = Factures.FkCustomer";
            SQLiteCommand commandFacture = new SQLiteCommand(sqlFacture, m_dbConnection);
            SQLiteDataReader readerFacture = commandFacture.ExecuteReader(); ;
            while (readerFacture.Read())
            {
                if (idProductHasFacture != Convert.ToInt32(readerFacture["idFacture"]))
                {
                    InfoFacture.Add(readerFacture["DateFacture"].ToString());
                    if (readerFacture["NameCustomer"].ToString().Length >= 5)
                    {
                        InfoFacture.Add(readerFacture["NameCustomer"].ToString());
                    }
                    else
                    {
                        InfoFacture.Add(readerFacture["NameCustomer"].ToString() + " ");
                    }
                    InfoFacture.Add((Convert.ToDouble(readerFacture["PriceTotalFacture"].ToString())/100).ToString());
                    if (Convert.ToInt32(readerFacture["ExportFacture"]) == 0)
                    {
                        InfoFacture.Add("Non");
                    }
                    else
                    {
                        InfoFacture.Add("Oui");
                    }
                    InfoFacture.Add(readerFacture["TauxTVA"].ToString());
                    idProductHasFacture = Convert.ToInt32(readerFacture["idFacture"]);
                }
            }

            return InfoFacture;
        }

        /// <summary>
        /// Get any informations of the product link with a facture
        /// </summary>
        /// <param name="frmModFacture">the form to add information in the listbox</param>
        /// <param name="date">the date of creation of the facture</param>
        /// <param name="name">the name of the product</param>
        /// <returns></returns>
        public void GetProductFacture(FrmModFacture frmModFacture, string date, string name)
        {
            name = name.Replace("'", " ");
            List<String> AllProduct = new List<string>();
            int idFacture = 0;
            string sqlFacture = "SELECT idFacture FROM Factures INNER JOIN ProductsHasFactures ON Factures.idFacture = ProductsHasFactures.fkFacture INNER JOIN Customers ON Customers.idCustomer = Factures.FkCustomer WHERE NameCustomer = '" + name + "' AND Datefacture = '" + date + "'";
            SQLiteCommand commandFacture = new SQLiteCommand(sqlFacture, m_dbConnection);
            SQLiteDataReader readerFacture = commandFacture.ExecuteReader(); ;
            while (readerFacture.Read())
            {
                idFacture = Convert.ToInt32(readerFacture["idFacture"]);
            }

            string sqlProduct = "SELECT QteProductFacture, LabelProduct, NameTypeProduct, PriceTotalProduct FROM ProductsHasFactures INNER JOIN Products ON ProductsHasFactures.FkProduct = Products.idProduct INNER JOIN TypeProducts ON Products.FkTypeProduct = TypeProducts.idTypeProduct WHERE FkFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sqlProduct, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                frmModFacture.LstProduitSelectedFacture.Items.Add(reader["LabelProduct"].ToString() + "\t" + reader["NameTypeProduct"].ToString() + "\t" + reader["QteProductFacture"].ToString() + "\t" + (Convert.ToDouble(reader["PriceTotalProduct"].ToString())/100).ToString() + " CHF");
            }
        }

        /// <summary>
        /// Get any informations of the product link with a facture
        /// </summary>
        /// <param name="date">the date of creation of the facture</param>
        /// <param name="name">the name of the product</param>
        /// <returns></returns>
        public List<string> GetProductFactureForExport(string date, string name)
        {
            name= name.Replace("'", " ");
            List<String> AllProduct = new List<string>();
            int idFacture = 0;
            string sqlFacture = "SELECT idFacture FROM Factures INNER JOIN ProductsHasFactures ON Factures.idFacture = ProductsHasFactures.fkFacture INNER JOIN Customers ON Customers.idCustomer = Factures.FkCustomer WHERE NameCustomer = '" + name + "' AND Datefacture = '" + date + "'";
            SQLiteCommand commandFacture = new SQLiteCommand(sqlFacture, m_dbConnection);
            SQLiteDataReader readerFacture = commandFacture.ExecuteReader(); ;
            while (readerFacture.Read())
            {
                idFacture = Convert.ToInt32(readerFacture["idFacture"]);
            }

            string sqlProduct = "SELECT QteProductFacture, LabelProduct, NameTypeProduct, PriceProduct FROM ProductsHasFactures INNER JOIN Products ON ProductsHasFactures.FkProduct = Products.idProduct INNER JOIN TypeProducts ON Products.FkTypeProduct = TypeProducts.idTypeProduct WHERE FkFacture = " + idFacture;
            SQLiteCommand command = new SQLiteCommand(sqlProduct, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader(); ;
            while (reader.Read())
            {
                AllProduct.Add(reader["LabelProduct"].ToString() + " (" + reader["NameTypeProduct"].ToString() + ")");
                AllProduct.Add(reader["QteProductFacture"].ToString());
                AllProduct.Add((Convert.ToDouble(reader["PriceProduct"].ToString())/100).ToString());
            }
            return AllProduct;
        }

        #endregion Facture

        #region Customer

        /// <summary>
        /// get the Firstname of the customer
        /// </summary>
        /// <param name="name"> The name of the customer search </param>
        /// <returns></returns>
        public string GetFirstNameCutomer(string name)
        {
            name = name.Replace("'", " ");
            string sql = "select FirstNameCustomer from Customers WHERE NameCustomer = '" + name + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            string firstname = "";
            while (reader.Read())
            {
                firstname = reader["FirstNameCustomer"].ToString();
            }

            return firstname;
        }

        /// <summary>
        /// get the name of the customer
        /// </summary>
        /// <returns></returns>
        public List<String> GetCustomerName()
        {
            string sql = "select NameCustomer, FirstNameCustomer from Customers";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            List<String> name = new List<String>();
            while (reader.Read())
            {
                name.Add(reader["FirstNameCustomer"].ToString() + " " + reader["NameCustomer"].ToString());
            }

            return name;
        }

        /// <summary>
        /// get all information of customer
        /// </summary>
        /// <returns></returns>
        public List<String> GetCustomer()
        {
            List<String> Info = new List<String>();
            string sql = "select NameCustomer, FirstNameCustomer, RueCustomer, NumRueCustomer, NPACustomer, VilleCustomer, EmailCustomer, TelCustomer from Customers";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Info.Add(reader["FirstNameCustomer"].ToString());   //Prénom
                Info.Add(reader["NameCustomer"].ToString());        //Nom
                Info.Add(reader["RueCustomer"].ToString());         //Rue
                Info.Add(reader["NumRueCustomer"].ToString());      //Numéro de rue
                Info.Add(reader["VilleCustomer"].ToString());       //Ville
                Info.Add(reader["NPACustomer"].ToString());         //NPA
                Info.Add(reader["EmailCustomer"].ToString());       //Email
                Info.Add(reader["TelCustomer"].ToString());         //PhoneNumbre
            }

            return Info;
        }

        /// <summary>
        /// get all information of customer according to his Name and Firstname
        /// </summary>
        /// <param name="name">name of the customer</param>
        /// <param name="firstname">firstname of the customer</param>
        /// <returns></returns>
        public List<String> GetCustomerInfo(String name, String firstname)
        {
            name = name.Replace("'", " ");
            firstname= firstname.Replace("'", " ");
            List<String> Info = new List<String>();
            string sql = "select RueCustomer, NumRueCustomer, NPACustomer, VilleCustomer, idCustomer, EmailCustomer, TelCustomer from Customers WHERE NameCustomer = '" + name + "' AND FirstNameCustomer = '" + firstname + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Info.Add(reader["RueCustomer"].ToString());         //Rue
                Info.Add(reader["NumRueCustomer"].ToString());      //Numéro de rue
                Info.Add(reader["NPACustomer"].ToString());         //NPA
                Info.Add(reader["VilleCustomer"].ToString());       //Ville
                Info.Add(reader["idCustomer"].ToString());          //Id
                Info.Add(reader["EmailCustomer"].ToString());          //Id
                Info.Add(reader["TelCustomer"].ToString());          //Id
            }

            return Info;
        }

        /// <summary>
        /// Update the information of Customer
        /// </summary>
        /// <param name="name">name of the customer</param>
        /// <param name="firstname">firstname of the customer</param>
        /// <param name="address">address of the customer</param>
        /// <param name="nbAddress">number of address of the customer</param>
        /// <param name="city">city of the customer</param>
        /// <param name="npa">npa of the customer</param>
        /// <returns></returns>
        public void UpdateCustomerInfo(int idCustomer, string name, string firstname, string address, int nbAddress, string city, int npa, string phone, string email)
        {
            name= name.Replace("'", " ");
            firstname = firstname.Replace("'", " ");
            address= address.Replace("'", " ");
            city= city.Replace("'", " ");
            phone= phone.Replace(".", " ");
            phone=phone.Replace("/", " ");
            string sql = "UPDATE Customers SET EmailCustomer = '" + email + "', TelCustomer = '" + phone + "', NameCustomer = '" + name +"', FirstNameCustomer = '" + firstname + "', RueCustomer = '" + address +"', NumRueCustomer = " + nbAddress +", NPACustomer = " + npa +", VilleCustomer = '" + city + "' WHERE idCustomer = " + idCustomer;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

        }

        /// <summary>
        /// Add a Customer
        /// </summary>
        /// <param name="name">name of the customer</param>
        /// <param name="firstname">firstname of the customer</param>
        /// <param name="address">address of the customer</param>
        /// <param name="nbAddress">number of address of the customer</param>
        /// <param name="city">city of the customer</param>
        /// <param name="npa">npa of the customer</param>
        /// <returns></returns>
        public void CreateCustomer(string name, string firstname, string address, int nbAddress, string city, int npa, string phone, string email)
        {
            name = name.Replace("'", " ");
            firstname = firstname.Replace(" ", "-");
            firstname = firstname.Replace("'", " ");
            address = address.Replace("\'", " ");
            city = city.Replace("'", " ");
            phone = phone.Replace(".", " ");
            phone = phone.Replace("/", " ");
            int id = 0;
            string sqlId = "SELECT idCustomer FROM Customers";
            SQLiteCommand commandId = new SQLiteCommand(sqlId, m_dbConnection);
            SQLiteDataReader readerId = commandId.ExecuteReader();

            while (readerId.Read())
            {
                id = Convert.ToInt32(readerId["idCustomer"]);
            }
            id += 1;

            name.Replace(" ", "-");

            string sql = "INSERT INTO Customers VALUES (" + id + ",'" + name + "', '" + firstname + "', '" + address + "', " + nbAddress + ", " + npa + ", '" + email + "', '" + phone + "', '" + city + "')";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

        }

        /// <summary>
        /// delete a Customer
        /// </summary>
        /// <param name="name">name of the customer</param>
        /// <param name="firstname">firstname of the customer</param>
        /// <returns></returns>
        public void DeleteCustomer(string name, string firstname)
        {
            name = name.Replace("'", " ");
            firstname = firstname.Replace("'", " ");
            string sql = "DELETE FROM Customers WHERE NameCustomer = '" + name + "' AND FirstNameCustomer = '" + firstname + "'";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        #endregion Customer

    }
}
