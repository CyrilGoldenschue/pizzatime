﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmModFacture : Form
    {
        ConnectionDB conn;
        public bool IsClosed = true;
        FrmAdd_SuppProduitFacture frmAdd_SuppProduitFacture;
        string FactureSelected = "";
        FrmGestionFactures FrmGestionFactures;
        string NameUser = "";
        int TVA = 0;
        int idFacture = 0;
        public FrmModFacture(string factureSelected, FrmGestionFactures frmGestionFactures)
        {
            InitializeComponent();
            FactureSelected = factureSelected;
            FrmGestionFactures = frmGestionFactures;
            RefreshFacture();
        }

        private void BtnUpdProduct_Click(object sender, EventArgs e)
        {
            //Vérifie si le formulaire ouvert a été fermé sinon le remet en premier plan
            if (IsClosed)
            {
                IsClosed = false;
                frmAdd_SuppProduitFacture = new FrmAdd_SuppProduitFacture(LstProduitSelectedFacture, LblDateCreation, CbxClientFacture, this);
                frmAdd_SuppProduitFacture.Show();
               
            }
            else
            {
                frmAdd_SuppProduitFacture.Focus();
            }
        }

        private void BtnValidationFacture_Click(object sender, EventArgs e)
        {
            TVA = Convert.ToInt32(Convert.ToDouble(LblTVA.Text)*10);
            conn = new ConnectionDB();
            string[] NewUser = CbxClientFacture.Text.Split(' ');
            int price = Convert.ToInt32(Convert.ToDouble(LblPriceFacture.Text.Substring(0, LblPriceFacture.Text.Length - 4)) * 100);
            idFacture = conn.GetIdFacture(LblDateCreation.Text, NameUser, conn.GetFirstNameCutomer(NameUser), price);
            conn.UpdateFacture(idFacture, NewUser[1], NewUser[0], Convert.ToDouble(LblPriceFacture.Text.Substring(0, LblPriceFacture.Text.Length - 4)), TVA);
            this.Close();
        }

        public void RefreshFacture()
        {
            conn = new ConnectionDB();
            List<String> NameCustomer = new List<String>();
            string[] LastWord;
            string Date;
            List<string> InfoFacture = new List<string>();

            InfoFacture = conn.GetFacture();

            LastWord = FactureSelected.Split(' ');
            Date = LastWord[0].ToString();
            LstProduitSelectedFacture.Items.Clear();

            
            if (LastWord.Count() == 16)
            {
                if (LastWord[12].Length <= 5)
                {
                    NameUser = LastWord[12];
                    LastWord = LastWord[15].Split('\t');
                }
            }
            else if (LastWord.Count() == 14)
            {
                if (LastWord[10].Length <= 5)
                {
                    NameUser = LastWord[10];
                    LastWord = LastWord[13].Split('\t');
                }
            }
            else
            {
                if (LastWord[LastWord.Count() - 1].Length <= 5)
                {
                    NameUser = LastWord[10];
                    LastWord = LastWord[11].Split('\t');
                }
                else
                {
                    if (LastWord[12] != "") { NameUser = LastWord[12]; }
                    if (LastWord[10] != "") { NameUser = LastWord[10]; }
                    LastWord = LastWord[LastWord.Count() - 1].Split('\t');
                    if (LastWord[0] != ""){ NameUser = LastWord[0]; }
                }
            }
            NameCustomer = conn.GetCustomerName();

            for (int i = 0; i < NameCustomer.Count(); i++)
            {
                CbxClientFacture.Items.Add(NameCustomer[i]);
            }

            for (int i = 0; i < InfoFacture.Count(); i += 5)
            {
                if (InfoFacture[0 + i] == Date && InfoFacture[1 + i].Replace(" ", "") == NameUser)
                {
                    LblPriceFacture.Text = InfoFacture[2 + i] + " CHF";
                    LblTVA.Text = (Convert.ToDouble(InfoFacture[4+i]) / 10).ToString();
                }
            }

            CbxClientFacture.SelectedItem = conn.GetFirstNameCutomer(NameUser) + " " + NameUser;
            //LblPriceFacture.Text = LastWord[1] + " CHF";
            conn.GetProductFacture(this, Date, NameUser);
            LblDateCreation.Text = Date;
            int price = Convert.ToInt32(Convert.ToDouble(LblPriceFacture.Text.Substring(0, LblPriceFacture.Text.Length - 4)) * 100);            //Convert.ToInt32(Convert.ToDouble(LblPriceFacture.Text.Substring(LblPriceFacture.Text.Length - 4)) * 100) ;
            idFacture = conn.GetIdFacture(LblDateCreation.Text, NameUser, conn.GetFirstNameCutomer(NameUser), price);

        }

        private void FrmModFacture_FormClosed(object sender, FormClosedEventArgs e)
        {
            FrmGestionFactures.IsClosed = true;
            FrmGestionFactures.RefreshListFacture();
        }

        private void BtnUpdateTVA_Click(object sender, EventArgs e)
        {
            conn = new ConnectionDB();
            FrmTVA frmTVA = new FrmTVA();
            frmTVA.ShowDialog();
            TVA = frmTVA.TVA;
            conn.UpdateTVAFacture(idFacture, TVA);
            RefreshFacture();
        }
    }
}
