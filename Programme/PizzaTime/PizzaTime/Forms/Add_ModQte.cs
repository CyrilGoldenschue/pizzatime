﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmAdd_ModQte : Form
    {
        ConnectionDB conn;
        string LabelProduct = "";
        public FrmAdd_ModQte(string labelProduct)
        {
            InitializeComponent();
            LabelProduct = labelProduct;
            LblQteProduit.Text = "Combien de quantité voulez-vous ajouter au produit " + LabelProduct;
        }

        private void BtnAddQte_Click(object sender, EventArgs e)
        {
            conn = new ConnectionDB();
            conn.addProductQte(LabelProduct, Convert.ToInt32(NbQty.Value));
            this.Close();
        }
    }
}
