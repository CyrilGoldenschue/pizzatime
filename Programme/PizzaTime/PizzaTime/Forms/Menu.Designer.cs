﻿namespace PizzaTime
{
    partial class FrmPizzaTimeMenu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPizzaTimeMenu));
            this.BtnSelect = new System.Windows.Forms.Button();
            this.LstProduitToSelect = new System.Windows.Forms.ListBox();
            this.LstProduitSelected = new System.Windows.Forms.ListBox();
            this.BtnUnselect = new System.Windows.Forms.Button();
            this.BtnCommande = new System.Windows.Forms.Button();
            this.NbQuantitéSelected = new System.Windows.Forms.NumericUpDown();
            this.BtnGestClients = new System.Windows.Forms.Button();
            this.BtnGestProduits = new System.Windows.Forms.Button();
            this.BtnGestFactures = new System.Windows.Forms.Button();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LblTotalPrice = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NbQuantitéSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnSelect
            // 
            this.BtnSelect.Location = new System.Drawing.Point(364, 217);
            this.BtnSelect.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSelect.Name = "BtnSelect";
            this.BtnSelect.Size = new System.Drawing.Size(80, 28);
            this.BtnSelect.TabIndex = 0;
            this.BtnSelect.Text = "------->";
            this.BtnSelect.UseVisualStyleBackColor = true;
            this.BtnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // LstProduitToSelect
            // 
            this.LstProduitToSelect.FormattingEnabled = true;
            this.LstProduitToSelect.ItemHeight = 16;
            this.LstProduitToSelect.Location = new System.Drawing.Point(16, 31);
            this.LstProduitToSelect.Margin = new System.Windows.Forms.Padding(4);
            this.LstProduitToSelect.Name = "LstProduitToSelect";
            this.LstProduitToSelect.Size = new System.Drawing.Size(299, 484);
            this.LstProduitToSelect.TabIndex = 1;
            // 
            // LstProduitSelected
            // 
            this.LstProduitSelected.FormattingEnabled = true;
            this.LstProduitSelected.ItemHeight = 16;
            this.LstProduitSelected.Location = new System.Drawing.Point(477, 31);
            this.LstProduitSelected.Margin = new System.Windows.Forms.Padding(4);
            this.LstProduitSelected.Name = "LstProduitSelected";
            this.LstProduitSelected.Size = new System.Drawing.Size(291, 452);
            this.LstProduitSelected.TabIndex = 2;
            // 
            // BtnUnselect
            // 
            this.BtnUnselect.Location = new System.Drawing.Point(364, 252);
            this.BtnUnselect.Margin = new System.Windows.Forms.Padding(4);
            this.BtnUnselect.Name = "BtnUnselect";
            this.BtnUnselect.Size = new System.Drawing.Size(80, 28);
            this.BtnUnselect.TabIndex = 3;
            this.BtnUnselect.Text = "<-------";
            this.BtnUnselect.UseVisualStyleBackColor = true;
            this.BtnUnselect.Click += new System.EventHandler(this.BtnUnselect_Click);
            // 
            // BtnCommande
            // 
            this.BtnCommande.Location = new System.Drawing.Point(477, 495);
            this.BtnCommande.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCommande.Name = "BtnCommande";
            this.BtnCommande.Size = new System.Drawing.Size(292, 28);
            this.BtnCommande.TabIndex = 4;
            this.BtnCommande.Text = "Commander";
            this.BtnCommande.UseVisualStyleBackColor = true;
            this.BtnCommande.Click += new System.EventHandler(this.BtnCommande_Click);
            // 
            // NbQuantitéSelected
            // 
            this.NbQuantitéSelected.Location = new System.Drawing.Point(364, 154);
            this.NbQuantitéSelected.Margin = new System.Windows.Forms.Padding(4);
            this.NbQuantitéSelected.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NbQuantitéSelected.Name = "NbQuantitéSelected";
            this.NbQuantitéSelected.Size = new System.Drawing.Size(80, 22);
            this.NbQuantitéSelected.TabIndex = 5;
            this.NbQuantitéSelected.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BtnGestClients
            // 
            this.BtnGestClients.Location = new System.Drawing.Point(825, 251);
            this.BtnGestClients.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGestClients.Name = "BtnGestClients";
            this.BtnGestClients.Size = new System.Drawing.Size(243, 28);
            this.BtnGestClients.TabIndex = 6;
            this.BtnGestClients.Text = "Gestion de Clients";
            this.BtnGestClients.UseVisualStyleBackColor = true;
            this.BtnGestClients.Click += new System.EventHandler(this.BtnGestClients_Click);
            // 
            // BtnGestProduits
            // 
            this.BtnGestProduits.Location = new System.Drawing.Point(825, 287);
            this.BtnGestProduits.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGestProduits.Name = "BtnGestProduits";
            this.BtnGestProduits.Size = new System.Drawing.Size(243, 28);
            this.BtnGestProduits.TabIndex = 7;
            this.BtnGestProduits.Text = "Gestion de Produits";
            this.BtnGestProduits.UseVisualStyleBackColor = true;
            this.BtnGestProduits.Click += new System.EventHandler(this.BtnGestProduits_Click);
            // 
            // BtnGestFactures
            // 
            this.BtnGestFactures.Location = new System.Drawing.Point(825, 322);
            this.BtnGestFactures.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGestFactures.Name = "BtnGestFactures";
            this.BtnGestFactures.Size = new System.Drawing.Size(243, 28);
            this.BtnGestFactures.TabIndex = 8;
            this.BtnGestFactures.Text = "Gestion de Factures";
            this.BtnGestFactures.UseVisualStyleBackColor = true;
            this.BtnGestFactures.Click += new System.EventHandler(this.BtnGestFactures_Click);
            // 
            // Logo
            // 
            this.Logo.InitialImage = null;
            this.Logo.Location = new System.Drawing.Point(841, 15);
            this.Logo.Margin = new System.Windows.Forms.Padding(4);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(227, 216);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 9;
            this.Logo.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(306, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nom                        Prix          Type      Quantité";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(769, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Prix total :";
            // 
            // LblTotalPrice
            // 
            this.LblTotalPrice.AutoSize = true;
            this.LblTotalPrice.Location = new System.Drawing.Point(769, 92);
            this.LblTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblTotalPrice.Name = "LblTotalPrice";
            this.LblTotalPrice.Size = new System.Drawing.Size(0, 17);
            this.LblTotalPrice.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(473, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(306, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nom                        Prix          Type      Quantité";
            // 
            // FrmPizzaTimeMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 522);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LblTotalPrice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.BtnGestFactures);
            this.Controls.Add(this.BtnGestProduits);
            this.Controls.Add(this.BtnGestClients);
            this.Controls.Add(this.NbQuantitéSelected);
            this.Controls.Add(this.BtnCommande);
            this.Controls.Add(this.BtnUnselect);
            this.Controls.Add(this.LstProduitSelected);
            this.Controls.Add(this.LstProduitToSelect);
            this.Controls.Add(this.BtnSelect);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1099, 569);
            this.MinimumSize = new System.Drawing.Size(1099, 569);
            this.Name = "FrmPizzaTimeMenu";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "PizzaTime Menu";
            ((System.ComponentModel.ISupportInitialize)(this.NbQuantitéSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnSelect;
        private System.Windows.Forms.ListBox LstProduitSelected;
        private System.Windows.Forms.Button BtnUnselect;
        private System.Windows.Forms.Button BtnCommande;
        private System.Windows.Forms.NumericUpDown NbQuantitéSelected;
        private System.Windows.Forms.Button BtnGestClients;
        private System.Windows.Forms.Button BtnGestProduits;
        private System.Windows.Forms.Button BtnGestFactures;
        private System.Windows.Forms.PictureBox Logo;
        public System.Windows.Forms.ListBox LstProduitToSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblTotalPrice;
        private System.Windows.Forms.Label label4;
    }
}

