﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmTVA : Form
    {
        public int TVA = 0;
        public bool Continue = true;
        public FrmTVA()
        {
            InitializeComponent();
        }

        private void BtnValidationTVA_Click(object sender, EventArgs e)
        {
            if (RdbTvaInPlace.Checked)
            {
                TVA = 77;
            }
            else
            {
                TVA = 25;
            }
            Continue = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Continue = false;
            this.Close();
        }
    }
}
