﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmAdd_ModProduit : Form
    {
        bool UpdateProduct;
        ConnectionDB conn;
        List<string> TypeProduct = new List<string>();
        int idProduct;

        public FrmAdd_ModProduit(bool upd, string label = "", string typeProduct = "", int price = 1, int qte = 1)
        {
            InitializeComponent();
            conn = new ConnectionDB();
            UpdateProduct = upd;
            TxtlabelProduit.Text = label;
            TypeProduct = conn.GetTypeProduct();
            NbPrice.Value = price;
            NbQte.Value = qte;
            idProduct = conn.GetIdProduct(label);

            for (int i = 0; i < TypeProduct.Count(); i++)
            {

                CmbTypeProduit.Items.Add(TypeProduct[i]);
            }

            CmbTypeProduit.SelectedItem = typeProduct;
        }

        private void BtnValidationProduit_Click(object sender, EventArgs e)
        {
            if (TxtlabelProduit.Text != "" || Convert.ToInt32(NbPrice.Value) != 0 || CmbTypeProduit.Text != "" || Convert.ToInt32(NbQte.Value) != 0)
            {
                conn = new ConnectionDB();
                if (UpdateProduct)
                {
                    conn.UpdateProduct(idProduct, TxtlabelProduit.Text, Convert.ToInt32(Convert.ToDouble(NbPrice.Value)*100), Convert.ToInt32(NbQte.Value), CmbTypeProduit.Text);
                    this.Close();
                }
                else
                {
                    conn.CreateProduct(TxtlabelProduit.Text, Convert.ToInt32(Convert.ToDouble(NbPrice.Value)*100), Convert.ToInt32(NbQte.Value), CmbTypeProduit.Text);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Un des champs n'est pas rempli veuillez le remplir s'il vous plaît.");
            }
        }
    }
}
