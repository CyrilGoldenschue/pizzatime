﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmAdd_ModClient : Form
    {
        int IdCustomer = 0;
        bool UpdateCustomer = true;
        ConnectionDB conn;
        public FrmAdd_ModClient(bool Upd ,int Id = 0, string name = "", string firstname = "", string address = "", int nbAddress = 0, string city = "", int npa = 0, string phone = "", string email="")
        {
            InitializeComponent();
            TxtFirstnameClient.Text = firstname;
            TxtNameClient.Text = name;
            TxtRueClient.Text = address;
            TxtVilleClient.Text = city;
            TxtEmailCustomer.Text = email;
            TxtPhoneCustomer.Text = phone;
            NbNPAClient.Value = npa;
            NbNumRueClient.Value = nbAddress;
            IdCustomer = Id;
            UpdateCustomer = Upd;
        }

        private void BtnValidationClient_Click(object sender, EventArgs e)
        {
            if (TxtNameClient.Text != "" || TxtFirstnameClient.Text != "" || TxtRueClient.Text != "" || Convert.ToInt32(NbNumRueClient.Value) != 0 || TxtVilleClient.Text != "" || Convert.ToInt32(NbNPAClient.Value) != 0)
            {
                conn = new ConnectionDB();
                if (UpdateCustomer)
                {
                    conn.UpdateCustomerInfo(IdCustomer, TxtNameClient.Text, TxtFirstnameClient.Text, TxtRueClient.Text, Convert.ToInt32(NbNumRueClient.Value), TxtVilleClient.Text, Convert.ToInt32(NbNPAClient.Value), TxtPhoneCustomer.Text, TxtEmailCustomer.Text);
                    this.Close();
                }
                else
                {
                    conn.CreateCustomer(TxtNameClient.Text, TxtFirstnameClient.Text, TxtRueClient.Text, Convert.ToInt32(NbNumRueClient.Value), TxtVilleClient.Text, Convert.ToInt32(NbNPAClient.Value), TxtPhoneCustomer.Text, TxtEmailCustomer.Text);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Un des champs n'est pas rempli veuillez le remplir s'il vous plaît.");
            }
        }

    }
}
