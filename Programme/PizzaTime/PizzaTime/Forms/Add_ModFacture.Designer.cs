﻿namespace PizzaTime
{
    partial class FrmModFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmModFacture));
            this.label1 = new System.Windows.Forms.Label();
            this.CbxClientFacture = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LstProduitSelectedFacture = new System.Windows.Forms.ListBox();
            this.BtnValidationFacture = new System.Windows.Forms.Button();
            this.BtnUpdProduct = new System.Windows.Forms.Button();
            this.LblDateCreation = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LblPriceFacture = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LblTVA = new System.Windows.Forms.Label();
            this.BtnUpdateTVA = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Client";
            // 
            // CbxClientFacture
            // 
            this.CbxClientFacture.FormattingEnabled = true;
            this.CbxClientFacture.Location = new System.Drawing.Point(17, 32);
            this.CbxClientFacture.Margin = new System.Windows.Forms.Padding(4);
            this.CbxClientFacture.Name = "CbxClientFacture";
            this.CbxClientFacture.Size = new System.Drawing.Size(404, 24);
            this.CbxClientFacture.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(444, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prix";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Liste des produits sélectionnés";
            // 
            // LstProduitSelectedFacture
            // 
            this.LstProduitSelectedFacture.FormattingEnabled = true;
            this.LstProduitSelectedFacture.ItemHeight = 16;
            this.LstProduitSelectedFacture.Location = new System.Drawing.Point(20, 96);
            this.LstProduitSelectedFacture.Margin = new System.Windows.Forms.Padding(4);
            this.LstProduitSelectedFacture.Name = "LstProduitSelectedFacture";
            this.LstProduitSelectedFacture.Size = new System.Drawing.Size(556, 244);
            this.LstProduitSelectedFacture.TabIndex = 5;
            // 
            // BtnValidationFacture
            // 
            this.BtnValidationFacture.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnValidationFacture.Location = new System.Drawing.Point(20, 350);
            this.BtnValidationFacture.Margin = new System.Windows.Forms.Padding(4);
            this.BtnValidationFacture.Name = "BtnValidationFacture";
            this.BtnValidationFacture.Size = new System.Drawing.Size(557, 28);
            this.BtnValidationFacture.TabIndex = 6;
            this.BtnValidationFacture.Text = "Valider";
            this.BtnValidationFacture.UseVisualStyleBackColor = true;
            this.BtnValidationFacture.Click += new System.EventHandler(this.BtnValidationFacture_Click);
            // 
            // BtnUpdProduct
            // 
            this.BtnUpdProduct.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnUpdProduct.Location = new System.Drawing.Point(433, 64);
            this.BtnUpdProduct.Margin = new System.Windows.Forms.Padding(4);
            this.BtnUpdProduct.Name = "BtnUpdProduct";
            this.BtnUpdProduct.Size = new System.Drawing.Size(144, 28);
            this.BtnUpdProduct.TabIndex = 7;
            this.BtnUpdProduct.Text = "Modifier les produits";
            this.BtnUpdProduct.UseVisualStyleBackColor = true;
            this.BtnUpdProduct.Click += new System.EventHandler(this.BtnUpdProduct_Click);
            // 
            // LblDateCreation
            // 
            this.LblDateCreation.AutoSize = true;
            this.LblDateCreation.Location = new System.Drawing.Point(279, 7);
            this.LblDateCreation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblDateCreation.Name = "LblDateCreation";
            this.LblDateCreation.Size = new System.Drawing.Size(0, 17);
            this.LblDateCreation.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(152, 7);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date de création :";
            // 
            // LblPriceFacture
            // 
            this.LblPriceFacture.AutoSize = true;
            this.LblPriceFacture.Location = new System.Drawing.Point(448, 36);
            this.LblPriceFacture.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblPriceFacture.Name = "LblPriceFacture";
            this.LblPriceFacture.Size = new System.Drawing.Size(0, 17);
            this.LblPriceFacture.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(513, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "TVA";
            // 
            // LblTVA
            // 
            this.LblTVA.AutoSize = true;
            this.LblTVA.Location = new System.Drawing.Point(520, 36);
            this.LblTVA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblTVA.Name = "LblTVA";
            this.LblTVA.Size = new System.Drawing.Size(0, 17);
            this.LblTVA.TabIndex = 12;
            // 
            // BtnUpdateTVA
            // 
            this.BtnUpdateTVA.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnUpdateTVA.Location = new System.Drawing.Point(272, 64);
            this.BtnUpdateTVA.Margin = new System.Windows.Forms.Padding(4);
            this.BtnUpdateTVA.Name = "BtnUpdateTVA";
            this.BtnUpdateTVA.Size = new System.Drawing.Size(149, 28);
            this.BtnUpdateTVA.TabIndex = 13;
            this.BtnUpdateTVA.Text = "Modifier la TVA";
            this.BtnUpdateTVA.UseVisualStyleBackColor = true;
            this.BtnUpdateTVA.Click += new System.EventHandler(this.BtnUpdateTVA_Click);
            // 
            // FrmModFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 380);
            this.Controls.Add(this.BtnUpdateTVA);
            this.Controls.Add(this.LblTVA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LblPriceFacture);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LblDateCreation);
            this.Controls.Add(this.BtnUpdProduct);
            this.Controls.Add(this.BtnValidationFacture);
            this.Controls.Add(this.LstProduitSelectedFacture);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CbxClientFacture);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(609, 427);
            this.MinimumSize = new System.Drawing.Size(609, 427);
            this.Name = "FrmModFacture";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Modification facture";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmModFacture_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnValidationFacture;
        private System.Windows.Forms.Button BtnUpdProduct;
        public System.Windows.Forms.ListBox LstProduitSelectedFacture;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox CbxClientFacture;
        public System.Windows.Forms.Label LblDateCreation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblTVA;
        private System.Windows.Forms.Button BtnUpdateTVA;
        public System.Windows.Forms.Label LblPriceFacture;
    }
}