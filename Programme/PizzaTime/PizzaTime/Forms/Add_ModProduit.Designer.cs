﻿namespace PizzaTime
{
    partial class FrmAdd_ModProduit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdd_ModProduit));
            this.TxtlabelProduit = new System.Windows.Forms.TextBox();
            this.NbQte = new System.Windows.Forms.NumericUpDown();
            this.CmbTypeProduit = new System.Windows.Forms.ComboBox();
            this.NbPrice = new System.Windows.Forms.NumericUpDown();
            this.BtnValidationProduit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NbQte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtlabelProduit
            // 
            this.TxtlabelProduit.Location = new System.Drawing.Point(16, 26);
            this.TxtlabelProduit.Margin = new System.Windows.Forms.Padding(4);
            this.TxtlabelProduit.Name = "TxtlabelProduit";
            this.TxtlabelProduit.Size = new System.Drawing.Size(364, 22);
            this.TxtlabelProduit.TabIndex = 0;
            // 
            // NbQte
            // 
            this.NbQte.Location = new System.Drawing.Point(204, 96);
            this.NbQte.Margin = new System.Windows.Forms.Padding(4);
            this.NbQte.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NbQte.Name = "NbQte";
            this.NbQte.Size = new System.Drawing.Size(77, 22);
            this.NbQte.TabIndex = 1;
            this.NbQte.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // CmbTypeProduit
            // 
            this.CmbTypeProduit.FormattingEnabled = true;
            this.CmbTypeProduit.Location = new System.Drawing.Point(16, 95);
            this.CmbTypeProduit.Margin = new System.Windows.Forms.Padding(4);
            this.CmbTypeProduit.Name = "CmbTypeProduit";
            this.CmbTypeProduit.Size = new System.Drawing.Size(160, 24);
            this.CmbTypeProduit.TabIndex = 3;
            // 
            // NbPrice
            // 
            this.NbPrice.DecimalPlaces = 2;
            this.NbPrice.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.NbPrice.Location = new System.Drawing.Point(308, 96);
            this.NbPrice.Margin = new System.Windows.Forms.Padding(4);
            this.NbPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NbPrice.Name = "NbPrice";
            this.NbPrice.Size = new System.Drawing.Size(72, 22);
            this.NbPrice.TabIndex = 4;
            this.NbPrice.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BtnValidationProduit
            // 
            this.BtnValidationProduit.Location = new System.Drawing.Point(16, 140);
            this.BtnValidationProduit.Margin = new System.Windows.Forms.Padding(4);
            this.BtnValidationProduit.Name = "BtnValidationProduit";
            this.BtnValidationProduit.Size = new System.Drawing.Size(364, 28);
            this.BtnValidationProduit.TabIndex = 5;
            this.BtnValidationProduit.Text = "Valider";
            this.BtnValidationProduit.UseVisualStyleBackColor = true;
            this.BtnValidationProduit.Click += new System.EventHandler(this.BtnValidationProduit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nom du produit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Type de produit";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(200, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Quantité";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Prix";
            // 
            // FrmAdd_ModProduit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 177);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnValidationProduit);
            this.Controls.Add(this.NbPrice);
            this.Controls.Add(this.CmbTypeProduit);
            this.Controls.Add(this.NbQte);
            this.Controls.Add(this.TxtlabelProduit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(413, 224);
            this.MinimumSize = new System.Drawing.Size(413, 224);
            this.Name = "FrmAdd_ModProduit";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ajout/Modification produit";
            ((System.ComponentModel.ISupportInitialize)(this.NbQte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtlabelProduit;
        private System.Windows.Forms.NumericUpDown NbQte;
        private System.Windows.Forms.ComboBox CmbTypeProduit;
        private System.Windows.Forms.NumericUpDown NbPrice;
        private System.Windows.Forms.Button BtnValidationProduit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}