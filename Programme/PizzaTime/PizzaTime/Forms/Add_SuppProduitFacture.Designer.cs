﻿namespace PizzaTime
{
    partial class FrmAdd_SuppProduitFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdd_SuppProduitFacture));
            this.LstProduitOutFacture = new System.Windows.Forms.ListBox();
            this.LstProduitInFacture = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NbQantity = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnAddProduit = new System.Windows.Forms.Button();
            this.BtnDelProduit = new System.Windows.Forms.Button();
            this.BtnValidation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NbQantity)).BeginInit();
            this.SuspendLayout();
            // 
            // LstProduitOutFacture
            // 
            this.LstProduitOutFacture.FormattingEnabled = true;
            this.LstProduitOutFacture.ItemHeight = 16;
            this.LstProduitOutFacture.Location = new System.Drawing.Point(16, 30);
            this.LstProduitOutFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstProduitOutFacture.Name = "LstProduitOutFacture";
            this.LstProduitOutFacture.Size = new System.Drawing.Size(388, 132);
            this.LstProduitOutFacture.TabIndex = 0;
            // 
            // LstProduitInFacture
            // 
            this.LstProduitInFacture.FormattingEnabled = true;
            this.LstProduitInFacture.ItemHeight = 16;
            this.LstProduitInFacture.Location = new System.Drawing.Point(16, 223);
            this.LstProduitInFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstProduitInFacture.Name = "LstProduitInFacture";
            this.LstProduitInFacture.Size = new System.Drawing.Size(388, 148);
            this.LstProduitInFacture.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Liste de produit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 203);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Liste de produits dans la facture";
            // 
            // NbQantity
            // 
            this.NbQantity.Location = new System.Drawing.Point(303, 183);
            this.NbQantity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbQantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NbQantity.Name = "NbQantity";
            this.NbQantity.Size = new System.Drawing.Size(85, 22);
            this.NbQantity.TabIndex = 4;
            this.NbQantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(299, 166);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Quantité";
            // 
            // BtnAddProduit
            // 
            this.BtnAddProduit.Image = ((System.Drawing.Image)(resources.GetObject("BtnAddProduit.Image")));
            this.BtnAddProduit.Location = new System.Drawing.Point(99, 170);
            this.BtnAddProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddProduit.Name = "BtnAddProduit";
            this.BtnAddProduit.Size = new System.Drawing.Size(100, 28);
            this.BtnAddProduit.TabIndex = 6;
            this.BtnAddProduit.UseVisualStyleBackColor = true;
            this.BtnAddProduit.Click += new System.EventHandler(this.BtnAddProduit_Click);
            // 
            // BtnDelProduit
            // 
            this.BtnDelProduit.Image = ((System.Drawing.Image)(resources.GetObject("BtnDelProduit.Image")));
            this.BtnDelProduit.Location = new System.Drawing.Point(207, 170);
            this.BtnDelProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnDelProduit.Name = "BtnDelProduit";
            this.BtnDelProduit.Size = new System.Drawing.Size(84, 28);
            this.BtnDelProduit.TabIndex = 7;
            this.BtnDelProduit.UseVisualStyleBackColor = true;
            this.BtnDelProduit.Click += new System.EventHandler(this.BtnDelProduit_Click);
            // 
            // BtnValidation
            // 
            this.BtnValidation.Location = new System.Drawing.Point(16, 379);
            this.BtnValidation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnValidation.Name = "BtnValidation";
            this.BtnValidation.Size = new System.Drawing.Size(389, 28);
            this.BtnValidation.TabIndex = 8;
            this.BtnValidation.Text = "Valider";
            this.BtnValidation.UseVisualStyleBackColor = true;
            this.BtnValidation.Click += new System.EventHandler(this.BtnValidation_Click);
            // 
            // FrmAdd_SuppProduitFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 407);
            this.Controls.Add(this.BtnValidation);
            this.Controls.Add(this.BtnDelProduit);
            this.Controls.Add(this.BtnAddProduit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NbQantity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LstProduitInFacture);
            this.Controls.Add(this.LstProduitOutFacture);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(437, 454);
            this.MinimumSize = new System.Drawing.Size(437, 454);
            this.Name = "FrmAdd_SuppProduitFacture";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Modification de facture";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmAdd_SuppProduitFacture_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.NbQantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LstProduitOutFacture;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NbQantity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnAddProduit;
        private System.Windows.Forms.Button BtnDelProduit;
        private System.Windows.Forms.Button BtnValidation;
        public System.Windows.Forms.ListBox LstProduitInFacture;
    }
}