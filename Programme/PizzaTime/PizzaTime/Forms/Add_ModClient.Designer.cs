﻿namespace PizzaTime
{
    partial class FrmAdd_ModClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdd_ModClient));
            this.TxtNameClient = new System.Windows.Forms.TextBox();
            this.NbNumRueClient = new System.Windows.Forms.NumericUpDown();
            this.BtnValidationClient = new System.Windows.Forms.Button();
            this.TxtFirstnameClient = new System.Windows.Forms.TextBox();
            this.TxtVilleClient = new System.Windows.Forms.TextBox();
            this.TxtRueClient = new System.Windows.Forms.TextBox();
            this.NbNPAClient = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEmailCustomer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPhoneCustomer = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.NbNumRueClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbNPAClient)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtNameClient
            // 
            this.TxtNameClient.Location = new System.Drawing.Point(16, 27);
            this.TxtNameClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNameClient.Name = "TxtNameClient";
            this.TxtNameClient.Size = new System.Drawing.Size(208, 22);
            this.TxtNameClient.TabIndex = 0;
            // 
            // NbNumRueClient
            // 
            this.NbNumRueClient.Location = new System.Drawing.Point(16, 144);
            this.NbNumRueClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbNumRueClient.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbNumRueClient.Name = "NbNumRueClient";
            this.NbNumRueClient.Size = new System.Drawing.Size(160, 22);
            this.NbNumRueClient.TabIndex = 1;
            // 
            // BtnValidationClient
            // 
            this.BtnValidationClient.Location = new System.Drawing.Point(16, 350);
            this.BtnValidationClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnValidationClient.Name = "BtnValidationClient";
            this.BtnValidationClient.Size = new System.Drawing.Size(480, 28);
            this.BtnValidationClient.TabIndex = 2;
            this.BtnValidationClient.Text = "Valider";
            this.BtnValidationClient.UseVisualStyleBackColor = true;
            this.BtnValidationClient.Click += new System.EventHandler(this.BtnValidationClient_Click);
            // 
            // TxtFirstnameClient
            // 
            this.TxtFirstnameClient.Location = new System.Drawing.Point(264, 27);
            this.TxtFirstnameClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFirstnameClient.Name = "TxtFirstnameClient";
            this.TxtFirstnameClient.Size = new System.Drawing.Size(232, 22);
            this.TxtFirstnameClient.TabIndex = 3;
            // 
            // TxtVilleClient
            // 
            this.TxtVilleClient.Location = new System.Drawing.Point(16, 203);
            this.TxtVilleClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtVilleClient.Name = "TxtVilleClient";
            this.TxtVilleClient.Size = new System.Drawing.Size(479, 22);
            this.TxtVilleClient.TabIndex = 4;
            // 
            // TxtRueClient
            // 
            this.TxtRueClient.Location = new System.Drawing.Point(16, 84);
            this.TxtRueClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRueClient.Name = "TxtRueClient";
            this.TxtRueClient.Size = new System.Drawing.Size(479, 22);
            this.TxtRueClient.TabIndex = 5;
            // 
            // NbNPAClient
            // 
            this.NbNPAClient.Location = new System.Drawing.Point(337, 144);
            this.NbNPAClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbNPAClient.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbNPAClient.Name = "NbNPAClient";
            this.NbNPAClient.Size = new System.Drawing.Size(160, 22);
            this.NbNPAClient.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(260, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Prénom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Rue";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 124);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Numéro de rue";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(333, 124);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "NPA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 183);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Ville/Village";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 240);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Email";
            // 
            // TxtEmailCustomer
            // 
            this.TxtEmailCustomer.Location = new System.Drawing.Point(16, 260);
            this.TxtEmailCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEmailCustomer.Name = "TxtEmailCustomer";
            this.TxtEmailCustomer.Size = new System.Drawing.Size(479, 22);
            this.TxtEmailCustomer.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 294);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "Téléphone";
            // 
            // TxtPhoneCustomer
            // 
            this.TxtPhoneCustomer.Location = new System.Drawing.Point(16, 314);
            this.TxtPhoneCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPhoneCustomer.Name = "TxtPhoneCustomer";
            this.TxtPhoneCustomer.Size = new System.Drawing.Size(479, 22);
            this.TxtPhoneCustomer.TabIndex = 15;
            // 
            // FrmAdd_ModClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 379);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtPhoneCustomer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtEmailCustomer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NbNPAClient);
            this.Controls.Add(this.TxtRueClient);
            this.Controls.Add(this.TxtVilleClient);
            this.Controls.Add(this.TxtFirstnameClient);
            this.Controls.Add(this.BtnValidationClient);
            this.Controls.Add(this.NbNumRueClient);
            this.Controls.Add(this.TxtNameClient);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(529, 426);
            this.MinimumSize = new System.Drawing.Size(529, 426);
            this.Name = "FrmAdd_ModClient";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ajout/Modification client";
            ((System.ComponentModel.ISupportInitialize)(this.NbNumRueClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbNPAClient)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtNameClient;
        private System.Windows.Forms.NumericUpDown NbNumRueClient;
        private System.Windows.Forms.Button BtnValidationClient;
        private System.Windows.Forms.TextBox TxtFirstnameClient;
        private System.Windows.Forms.TextBox TxtVilleClient;
        private System.Windows.Forms.TextBox TxtRueClient;
        private System.Windows.Forms.NumericUpDown NbNPAClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtEmailCustomer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtPhoneCustomer;
    }
}