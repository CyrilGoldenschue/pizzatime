﻿namespace PizzaTime
{
    partial class FrmTVA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTVA));
            this.RdbTvaTakeAway = new System.Windows.Forms.RadioButton();
            this.RdbTvaInPlace = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnValidationTVA = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RdbTvaTakeAway
            // 
            this.RdbTvaTakeAway.AutoSize = true;
            this.RdbTvaTakeAway.Location = new System.Drawing.Point(14, 33);
            this.RdbTvaTakeAway.Name = "RdbTvaTakeAway";
            this.RdbTvaTakeAway.Size = new System.Drawing.Size(99, 21);
            this.RdbTvaTakeAway.TabIndex = 0;
            this.RdbTvaTakeAway.TabStop = true;
            this.RdbTvaTakeAway.Text = "À emporter";
            this.RdbTvaTakeAway.UseVisualStyleBackColor = true;
            // 
            // RdbTvaInPlace
            // 
            this.RdbTvaInPlace.AutoSize = true;
            this.RdbTvaInPlace.Location = new System.Drawing.Point(14, 60);
            this.RdbTvaInPlace.Name = "RdbTvaInPlace";
            this.RdbTvaInPlace.Size = new System.Drawing.Size(89, 21);
            this.RdbTvaInPlace.TabIndex = 1;
            this.RdbTvaInPlace.TabStop = true;
            this.RdbTvaInPlace.Text = "Sur place";
            this.RdbTvaInPlace.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Commande à emporter ou à manger sur place ?\r\n";
            // 
            // BtnValidationTVA
            // 
            this.BtnValidationTVA.Location = new System.Drawing.Point(12, 87);
            this.BtnValidationTVA.Name = "BtnValidationTVA";
            this.BtnValidationTVA.Size = new System.Drawing.Size(161, 29);
            this.BtnValidationTVA.TabIndex = 3;
            this.BtnValidationTVA.Text = "Valider";
            this.BtnValidationTVA.UseVisualStyleBackColor = true;
            this.BtnValidationTVA.Click += new System.EventHandler(this.BtnValidationTVA_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(182, 87);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(161, 29);
            this.BtnCancel.TabIndex = 4;
            this.BtnCancel.Text = "Annuler";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // FrmTVA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 131);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnValidationTVA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RdbTvaInPlace);
            this.Controls.Add(this.RdbTvaTakeAway);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTVA";
            this.Text = "TVA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton RdbTvaTakeAway;
        private System.Windows.Forms.RadioButton RdbTvaInPlace;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnValidationTVA;
        private System.Windows.Forms.Button BtnCancel;
    }
}