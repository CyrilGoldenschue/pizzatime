﻿namespace PizzaTime
{
    partial class FrmGestionProduits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGestionProduits));
            this.BtnModProduit = new System.Windows.Forms.Button();
            this.BtnSuppProduit = new System.Windows.Forms.Button();
            this.LstProduits = new System.Windows.Forms.ListBox();
            this.BtnAddProduit = new System.Windows.Forms.Button();
            this.BtnAddQte = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnModProduit
            // 
            this.BtnModProduit.Location = new System.Drawing.Point(183, 366);
            this.BtnModProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnModProduit.Name = "BtnModProduit";
            this.BtnModProduit.Size = new System.Drawing.Size(159, 58);
            this.BtnModProduit.TabIndex = 11;
            this.BtnModProduit.Text = "Modifier un produit";
            this.BtnModProduit.UseVisualStyleBackColor = true;
            this.BtnModProduit.Click += new System.EventHandler(this.BtnModProduit_Click);
            // 
            // BtnSuppProduit
            // 
            this.BtnSuppProduit.Location = new System.Drawing.Point(183, 300);
            this.BtnSuppProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSuppProduit.Name = "BtnSuppProduit";
            this.BtnSuppProduit.Size = new System.Drawing.Size(159, 58);
            this.BtnSuppProduit.TabIndex = 10;
            this.BtnSuppProduit.Text = "Supprimer un produit";
            this.BtnSuppProduit.UseVisualStyleBackColor = true;
            this.BtnSuppProduit.Click += new System.EventHandler(this.BtnSuppProduit_Click);
            // 
            // LstProduits
            // 
            this.LstProduits.FormattingEnabled = true;
            this.LstProduits.ItemHeight = 16;
            this.LstProduits.Location = new System.Drawing.Point(16, 31);
            this.LstProduits.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstProduits.Name = "LstProduits";
            this.LstProduits.Size = new System.Drawing.Size(324, 260);
            this.LstProduits.TabIndex = 9;
            // 
            // BtnAddProduit
            // 
            this.BtnAddProduit.Location = new System.Drawing.Point(16, 300);
            this.BtnAddProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddProduit.Name = "BtnAddProduit";
            this.BtnAddProduit.Size = new System.Drawing.Size(159, 58);
            this.BtnAddProduit.TabIndex = 8;
            this.BtnAddProduit.Text = "Ajouter un produit";
            this.BtnAddProduit.UseVisualStyleBackColor = true;
            this.BtnAddProduit.Click += new System.EventHandler(this.BtnAddProduit_Click);
            // 
            // BtnAddQte
            // 
            this.BtnAddQte.Location = new System.Drawing.Point(16, 366);
            this.BtnAddQte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddQte.Name = "BtnAddQte";
            this.BtnAddQte.Size = new System.Drawing.Size(159, 58);
            this.BtnAddQte.TabIndex = 12;
            this.BtnAddQte.Text = "Ajouter de la quantité";
            this.BtnAddQte.UseVisualStyleBackColor = true;
            this.BtnAddQte.Click += new System.EventHandler(this.BtnAddQte_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(285, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Label                       Prix          Type        Qte";
            // 
            // FrmGestionProduits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 428);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnAddQte);
            this.Controls.Add(this.BtnModProduit);
            this.Controls.Add(this.BtnSuppProduit);
            this.Controls.Add(this.LstProduits);
            this.Controls.Add(this.BtnAddProduit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(377, 475);
            this.MinimumSize = new System.Drawing.Size(377, 475);
            this.Name = "FrmGestionProduits";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GestionProduits";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnModProduit;
        private System.Windows.Forms.Button BtnSuppProduit;
        private System.Windows.Forms.ListBox LstProduits;
        private System.Windows.Forms.Button BtnAddProduit;
        private System.Windows.Forms.Button BtnAddQte;
        private System.Windows.Forms.Label label1;
    }
}