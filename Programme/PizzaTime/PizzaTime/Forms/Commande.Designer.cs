﻿namespace PizzaTime.Forms
{
    partial class FrmCommande
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCommande));
            this.RbtnCustomerExist = new System.Windows.Forms.RadioButton();
            this.RbtnCustomerNExist = new System.Windows.Forms.RadioButton();
            this.TxtNameCustomer = new System.Windows.Forms.TextBox();
            this.TxtFirstnameCustomer = new System.Windows.Forms.TextBox();
            this.TxtAddressCustomer = new System.Windows.Forms.TextBox();
            this.TxtCityCustomer = new System.Windows.Forms.TextBox();
            this.NbAddressCustomer = new System.Windows.Forms.NumericUpDown();
            this.NbNPA = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnValidationCommand = new System.Windows.Forms.Button();
            this.CbxCustomer = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtEmailCustomer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPhoneCustomer = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.NbAddressCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbNPA)).BeginInit();
            this.SuspendLayout();
            // 
            // RbtnCustomerExist
            // 
            this.RbtnCustomerExist.AutoSize = true;
            this.RbtnCustomerExist.Location = new System.Drawing.Point(17, 15);
            this.RbtnCustomerExist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RbtnCustomerExist.Name = "RbtnCustomerExist";
            this.RbtnCustomerExist.Size = new System.Drawing.Size(116, 21);
            this.RbtnCustomerExist.TabIndex = 0;
            this.RbtnCustomerExist.TabStop = true;
            this.RbtnCustomerExist.Text = "Client existant";
            this.RbtnCustomerExist.UseVisualStyleBackColor = true;
            this.RbtnCustomerExist.CheckedChanged += new System.EventHandler(this.RbtnCustomerExist_CheckedChanged);
            // 
            // RbtnCustomerNExist
            // 
            this.RbtnCustomerNExist.AutoSize = true;
            this.RbtnCustomerNExist.Location = new System.Drawing.Point(145, 15);
            this.RbtnCustomerNExist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RbtnCustomerNExist.Name = "RbtnCustomerNExist";
            this.RbtnCustomerNExist.Size = new System.Drawing.Size(144, 21);
            this.RbtnCustomerNExist.TabIndex = 1;
            this.RbtnCustomerNExist.TabStop = true;
            this.RbtnCustomerNExist.Text = "Client non existant";
            this.RbtnCustomerNExist.UseVisualStyleBackColor = true;
            this.RbtnCustomerNExist.CheckedChanged += new System.EventHandler(this.RbtnCustomerNExist_CheckedChanged);
            // 
            // TxtNameCustomer
            // 
            this.TxtNameCustomer.Location = new System.Drawing.Point(17, 65);
            this.TxtNameCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNameCustomer.Name = "TxtNameCustomer";
            this.TxtNameCustomer.Size = new System.Drawing.Size(245, 22);
            this.TxtNameCustomer.TabIndex = 2;
            // 
            // TxtFirstnameCustomer
            // 
            this.TxtFirstnameCustomer.Location = new System.Drawing.Point(272, 65);
            this.TxtFirstnameCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFirstnameCustomer.Name = "TxtFirstnameCustomer";
            this.TxtFirstnameCustomer.Size = new System.Drawing.Size(207, 22);
            this.TxtFirstnameCustomer.TabIndex = 3;
            // 
            // TxtAddressCustomer
            // 
            this.TxtAddressCustomer.Location = new System.Drawing.Point(16, 124);
            this.TxtAddressCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtAddressCustomer.Name = "TxtAddressCustomer";
            this.TxtAddressCustomer.Size = new System.Drawing.Size(376, 22);
            this.TxtAddressCustomer.TabIndex = 4;
            // 
            // TxtCityCustomer
            // 
            this.TxtCityCustomer.Location = new System.Drawing.Point(16, 183);
            this.TxtCityCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtCityCustomer.Name = "TxtCityCustomer";
            this.TxtCityCustomer.Size = new System.Drawing.Size(376, 22);
            this.TxtCityCustomer.TabIndex = 5;
            // 
            // NbAddressCustomer
            // 
            this.NbAddressCustomer.Location = new System.Drawing.Point(401, 124);
            this.NbAddressCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbAddressCustomer.Name = "NbAddressCustomer";
            this.NbAddressCustomer.Size = new System.Drawing.Size(79, 22);
            this.NbAddressCustomer.TabIndex = 6;
            // 
            // NbNPA
            // 
            this.NbNPA.Location = new System.Drawing.Point(401, 183);
            this.NbNPA.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbNPA.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NbNPA.Name = "NbNPA";
            this.NbNPA.Size = new System.Drawing.Size(79, 22);
            this.NbNPA.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(268, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Prénom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Adresse";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(397, 103);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "N°";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 164);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ville/Village";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(397, 164);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "NPA";
            // 
            // BtnValidationCommand
            // 
            this.BtnValidationCommand.Location = new System.Drawing.Point(16, 318);
            this.BtnValidationCommand.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnValidationCommand.Name = "BtnValidationCommand";
            this.BtnValidationCommand.Size = new System.Drawing.Size(464, 28);
            this.BtnValidationCommand.TabIndex = 14;
            this.BtnValidationCommand.Text = "Valider la commande";
            this.BtnValidationCommand.UseVisualStyleBackColor = true;
            this.BtnValidationCommand.Click += new System.EventHandler(this.BtnValidationCommand_Click);
            // 
            // CbxCustomer
            // 
            this.CbxCustomer.Enabled = false;
            this.CbxCustomer.FormattingEnabled = true;
            this.CbxCustomer.Location = new System.Drawing.Point(301, 15);
            this.CbxCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CbxCustomer.Name = "CbxCustomer";
            this.CbxCustomer.Size = new System.Drawing.Size(177, 24);
            this.CbxCustomer.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 214);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Email";
            // 
            // TxtEmailCustomer
            // 
            this.TxtEmailCustomer.Location = new System.Drawing.Point(16, 234);
            this.TxtEmailCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEmailCustomer.Name = "TxtEmailCustomer";
            this.TxtEmailCustomer.Size = new System.Drawing.Size(463, 22);
            this.TxtEmailCustomer.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 266);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Phone";
            // 
            // TxtPhoneCustomer
            // 
            this.TxtPhoneCustomer.Location = new System.Drawing.Point(17, 286);
            this.TxtPhoneCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPhoneCustomer.Name = "TxtPhoneCustomer";
            this.TxtPhoneCustomer.Size = new System.Drawing.Size(461, 22);
            this.TxtPhoneCustomer.TabIndex = 18;
            // 
            // FrmCommande
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 350);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtPhoneCustomer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtEmailCustomer);
            this.Controls.Add(this.CbxCustomer);
            this.Controls.Add(this.BtnValidationCommand);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NbNPA);
            this.Controls.Add(this.NbAddressCustomer);
            this.Controls.Add(this.TxtCityCustomer);
            this.Controls.Add(this.TxtAddressCustomer);
            this.Controls.Add(this.TxtFirstnameCustomer);
            this.Controls.Add(this.TxtNameCustomer);
            this.Controls.Add(this.RbtnCustomerNExist);
            this.Controls.Add(this.RbtnCustomerExist);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(511, 397);
            this.MinimumSize = new System.Drawing.Size(511, 397);
            this.Name = "FrmCommande";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Commande";
            ((System.ComponentModel.ISupportInitialize)(this.NbAddressCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NbNPA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton RbtnCustomerExist;
        private System.Windows.Forms.RadioButton RbtnCustomerNExist;
        private System.Windows.Forms.TextBox TxtNameCustomer;
        private System.Windows.Forms.TextBox TxtFirstnameCustomer;
        private System.Windows.Forms.TextBox TxtAddressCustomer;
        private System.Windows.Forms.TextBox TxtCityCustomer;
        private System.Windows.Forms.NumericUpDown NbAddressCustomer;
        private System.Windows.Forms.NumericUpDown NbNPA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BtnValidationCommand;
        private System.Windows.Forms.ComboBox CbxCustomer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtEmailCustomer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtPhoneCustomer;
    }
}