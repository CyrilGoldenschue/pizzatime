﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmAdd_SuppProduitFacture : Form
    {
        ConnectionDB conn;
        List<string> InfoProduct = new List<string>();
        ListBox ListProductInFacture;
        Label DateCreationFacture;
        ComboBox NamePerson;
        FrmModFacture FrmModFacture;
        public FrmAdd_SuppProduitFacture(ListBox listProductInFacture, Label dateFacture, ComboBox person, FrmModFacture frmModFacture)
        {
            InitializeComponent();
            FrmModFacture = frmModFacture;
            ListProductInFacture = listProductInFacture;
            DateCreationFacture = dateFacture;
            NamePerson = person;

            RefreshAllList();


        }

        private void RefreshListOutProduct()
        {
            conn = new ConnectionDB();
            LstProduitOutFacture.Items.Clear();

            InfoProduct = conn.GetProduct();

            for (int i = 0; i < InfoProduct.Count(); i += 4)
            {
                LstProduitOutFacture.Items.Add(InfoProduct[0 + i] + "\t" + InfoProduct[1 + i] + " CHF" + "\t" + InfoProduct[3 + i] + "\t" + InfoProduct[2 + i]);
            }
        }

        private void RefreshAllList()
        {
            conn = new ConnectionDB();
            LstProduitOutFacture.Items.Clear();

            InfoProduct = conn.GetProduct();
            foreach (object element in ListProductInFacture.Items)
            {
                LstProduitInFacture.Items.Add(element);
            }

            for (int i = 0; i < InfoProduct.Count(); i += 4)
            {
                LstProduitOutFacture.Items.Add(InfoProduct[0 + i] + "\t" + InfoProduct[1 + i] + " CHF" + "\t" + InfoProduct[3 + i] + "\t" + InfoProduct[2 + i]);
            }
        }

        private void BtnAddProduit_Click(object sender, EventArgs e)
        {
            if (LstProduitOutFacture.SelectedIndex >= 0)
            {
                conn = new ConnectionDB();
                string[] Words;
                string[] Product;
                int idFacture;
                double PriceProduct;
                bool IsntExist = true;
                string[] FirstWordSelect = LstProduitOutFacture.SelectedItem.ToString().Split('\t');
                int IndexList = 0;
                int Quantity = 0;
                double Price = 0;

                for (int i = 0; i < LstProduitInFacture.Items.Count; i++)
                {
                    string[] FirstWord = LstProduitInFacture.Items[i].ToString().Split('\t');
                    if (FirstWord[0] == FirstWordSelect[0])
                    {
                        IsntExist = false;
                        IndexList = i;
                        Quantity = Convert.ToInt32(FirstWord[2]);
                        Price = Convert.ToDouble(FirstWord[3].Substring(0, FirstWord[3].Length - 4));
                        break;
                    }
                }

                Words = NamePerson.Text.Split(' ');
                int price = Convert.ToInt32(Convert.ToDouble(FrmModFacture.LblPriceFacture.Text.Substring(0, FrmModFacture.LblPriceFacture.Text.Length - 4)) * 100);
                idFacture = conn.GetIdFacture(DateCreationFacture.Text, Words[1], Words[0], price);

                Product = LstProduitOutFacture.SelectedItem.ToString().Split('\t');
                Product[1] = Product[1].Substring(0, Product[1].Length - 4);
                PriceProduct = (Convert.ToInt32(NbQantity.Value) * (Convert.ToDouble(Product[1])*100))/100;

                if (IsntExist)
                {
                    if (Convert.ToInt32(Product[3]) >= NbQantity.Value)
                    {
                        conn.CreateProductHasFactures(idFacture, Product[0], Convert.ToInt32(NbQantity.Value), PriceProduct, true);
                        LstProduitInFacture.Items.Add(Product[0] + "\t" + Product[2] + "\t" + NbQantity.Value.ToString() + "\t" + PriceProduct.ToString() + " CHF");
                        conn.DeleteQtyProduct(conn.GetIdProduct(Product[0]), Convert.ToInt32(NbQantity.Value));
                    }
                    else
                    {
                        MessageBox.Show("Il n'y a pas assez de ce produit pour l'ajouter à la commande");
                    }
                }
                else
                {
                    if (Convert.ToInt32(Product[3]) >= NbQantity.Value + Quantity)
                    {
                        conn.UpdateProductHasFacture(idFacture, Product[0], Convert.ToInt32(NbQantity.Value), PriceProduct);
                        LstProduitInFacture.Items[IndexList] = Product[0] + "\t" + Product[2] + "\t" + (Quantity + NbQantity.Value).ToString() + "\t" + (PriceProduct + Price).ToString() + " CHF";
                        conn.DeleteQtyProduct(conn.GetIdProduct(Product[0]), Convert.ToInt32(NbQantity.Value));
                    }
                    else
                    {
                        MessageBox.Show("Il n'y a pas assez de ce produit pour l'ajouter à la commande");
                    }
                }
                NbQantity.Value = 1;
                RefreshListOutProduct();
            }
        }

        private void BtnDelProduit_Click(object sender, EventArgs e)
        {
            if (LstProduitInFacture.SelectedIndex >= 0)
            {
                conn = new ConnectionDB();
                string[] Words;
                string[] Product;
                int idFacture;
                double PriceProduct;

                Words = NamePerson.Text.Split(' ');
                idFacture = conn.GetIdFacture(DateCreationFacture.Text, Words[1], Words[0], Convert.ToInt32(FrmModFacture.LblPriceFacture.Text)*100);

                Product = LstProduitInFacture.SelectedItem.ToString().Split('\t');
                PriceProduct = Convert.ToDouble(Product[3].Substring(0, Product[3].Length - 4));

                conn.DeleteProductHasFacture(idFacture, Product[0], PriceProduct);
                conn.addProductQte(Product[0], Convert.ToInt32(Product[2]));
                RefreshListOutProduct();
                LstProduitInFacture.Items.Remove(LstProduitInFacture.SelectedItem);

            }
        }

        private void BtnValidation_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAdd_SuppProduitFacture_FormClosed(object sender, FormClosedEventArgs e)
        {
            FrmModFacture.IsClosed = true;
            FrmModFacture.RefreshFacture();
        }
    }
}
