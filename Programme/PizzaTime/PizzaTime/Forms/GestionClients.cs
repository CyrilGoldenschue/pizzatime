﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmGestionClients : Form
    {
        ConnectionDB conn;
        public FrmGestionClients()
        {
            InitializeComponent();
            RefreshListCustomer();
        }

        private void BtnAddClient_Click(object sender, EventArgs e)
        {
            
            FrmAdd_ModClient frmAdd_ModClient = new FrmAdd_ModClient(false);
            frmAdd_ModClient.ShowDialog();

            RefreshListCustomer();

        }

        private void BtnModClient_Click(object sender, EventArgs e)
        {
            conn = new ConnectionDB();
            string[] LastWord;
            List<string> Info = new List<string>();

            if (LstClients.SelectedIndex >= 0)
            {
                LastWord = LstClients.SelectedItem.ToString().Split('\t');
                if (LastWord.Count() == 2)
                {
                    Info = conn.GetCustomerInfo(LastWord[0], LastWord[1]);
                }
                else
                {
                    Info = conn.GetCustomerInfo(LastWord[0], LastWord[2]);
                }

                //MessageBox.Show(Info[0] +" "+ Info[1] + " " + Info[3] + " " + Info[2]);
                //this.Close();
                FrmAdd_ModClient frmAdd_ModClient;
                if (LastWord.Count() == 2)
                {
                    frmAdd_ModClient = new FrmAdd_ModClient(true, Convert.ToInt16(Info[4]), LastWord[0], LastWord[1], Info[0], Convert.ToInt16(Info[1]), Info[3], Convert.ToInt16(Info[2]), Info[6], Info[5]);
                }
                else
                {
                    frmAdd_ModClient = new FrmAdd_ModClient(true, Convert.ToInt16(Info[4]), LastWord[0], LastWord[2], Info[0], Convert.ToInt16(Info[1]), Info[3], Convert.ToInt16(Info[2]), Info[6], Info[5]);

                }
                frmAdd_ModClient.ShowDialog();

                RefreshListCustomer();

            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        private void BtnSuppClient_Click(object sender, EventArgs e)
        {
            if (LstClients.SelectedIndex >= 0)
            {
                conn = new ConnectionDB();
                string[] LastWord;

                LastWord = LstClients.SelectedItem.ToString().Split('\t');
                if (LastWord.Count() == 2)
                {
                    conn.DeleteCustomer(LastWord[0], LastWord[1]);
                }
                else
                {
                    conn.DeleteCustomer(LastWord[0], LastWord[2]);
                }

                RefreshListCustomer();
            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        private void RefreshListCustomer()
        {
            conn = new ConnectionDB();
            List<String> Customer = conn.GetCustomer();
            LstClients.Items.Clear();
            for (int i = 0; i<Customer.Count(); i+= 8) { 
                if (Customer[1 + i].Length >= 10)
                {
                    LstClients.Items.Add(Customer[1 + i] + "\t" + Customer[0 + i]);
                }
                else
                {
                    LstClients.Items.Add(Customer[1 + i] + "\t\t" + Customer[0 + i]);
                }
            }
        }
    }
}
