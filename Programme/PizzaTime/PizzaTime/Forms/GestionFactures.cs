﻿using GemBox.Spreadsheet;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmGestionFactures : Form
    {
        ConnectionDB conn;
        public bool IsClosed = true;
        FrmModFacture frmModFacture;
        bool Enable = true;
        Microsoft.Office.Interop.Excel.Application appli;
        Microsoft.Office.Interop.Excel._Workbook classeur;
        Microsoft.Office.Interop.Excel._Worksheet feuille;
        object M = System.Reflection.Missing.Value;

        public FrmGestionFactures()
        {
            InitializeComponent();
            RefreshListFacture();
        }

        //Update the facture selected
        private void BtnModFacture_Click(object sender, EventArgs e)
        {
            if (IsClosed)
            {
                if (LstFactures.SelectedIndex >= 0)
                {
                    IsClosed = false;
                    frmModFacture = new FrmModFacture(LstFactures.SelectedItem.ToString(), this);
                    frmModFacture.Show();
                }
                else
                {
                    MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
                }
            }
            else
            {
                frmModFacture.Focus();
            }
            RefreshListFacture();
        }

        //Delete the facture selected
        private void BtnSuppFacture_Click(object sender, EventArgs e)
        {
            conn = new ConnectionDB();
            string NameUser = "";
            if (LstFactures.SelectedIndex >= 0)
            {
                string[] Words = LstFactures.SelectedItem.ToString().Split(' ');
                string DateFacture = Words[0];
                int PrixFacture = 0;
                if (Words.Count() == 16)
                {
                    if (Words[12].Length <= 5)
                    {
                        NameUser = Words[12];
                        Words = Words[15].Split('\t');
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                }
                else if (Words.Count() == 14)
                {
                    if (Words[10].Length <= 5)
                    {
                        NameUser = Words[10];
                        Words = Words[13].Split('\t');
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                }
                else
                {
                    if (Words[Words.Count()-1].Length <= 5)
                    {
                        Words = Words[11].Split('\t');
                        NameUser = Words[10];
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                    else
                    {
                        if (Words[12] != "") { NameUser = Words[12]; }
                        if (Words[10] != "") { NameUser = Words[10]; }
                        Words = Words[Words.Count() - 1].Split('\t');
                        if (Words[0] != "") { NameUser = Words[0]; }
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                }
                conn.DeleteFacture(conn.GetIdFacture(DateFacture, NameUser, conn.GetFirstNameCutomer(NameUser), PrixFacture));
                RefreshListFacture();
            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        //Export the facture selected
        private void BtnExportFacture_Click(object sender, EventArgs e)
        {
            conn = new ConnectionDB();
            string NameUser = "";
            string IdFacture = "";
            List<string> InfoCustomer = new List<string>();
            List<string> InfoFacture = new List<string>();
            double TVA = 0;

            if (LstFactures.SelectedIndex >= 0)
            {
                string[] Words = LstFactures.SelectedItem.ToString().Split(' ');
                string DateFacture = Words[0];
                int PrixFacture = 0;
                if (Words.Count() == 16)
                {
                    if (Words[12].Length <= 5)
                    {
                        NameUser = Words[12];
                        Words = Words[15].Split('\t');
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                }
                else if (Words.Count() == 14)
                {
                    if (Words[10].Length <= 5)
                    {
                        NameUser = Words[10];
                        Words = Words[13].Split('\t');
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                }
                else
                {
                    if (Words[Words.Count() - 1].Length <= 5)
                    {
                        Words = Words[Words.Count() - 1].Split('\t');
                        NameUser = Words[0];
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                    }
                    else
                    {
                        if(Words[12] != "") { NameUser = Words[12]; }
                        if(Words[10] != "") { NameUser = Words[10]; }
                        Words = Words[Words.Count() - 1].Split('\t');
                        PrixFacture = Convert.ToInt32(Convert.ToDouble(Words[1]) * 100);
                        if (Words[0] != "") { NameUser = Words[0]; }
                    }
                }

                InfoCustomer = conn.GetCustomerInfo(NameUser, conn.GetFirstNameCutomer(NameUser));
                InfoFacture = conn.GetProductFactureForExport(DateFacture, NameUser);
                IdFacture = conn.GetIdFacture(DateFacture, NameUser, conn.GetFirstNameCutomer(NameUser), PrixFacture).ToString();
                TVA = Convert.ToDouble(conn.GetTVAFacture(Convert.ToInt32(IdFacture)))/1000;

                appli = new Microsoft.Office.Interop.Excel.Application();
                appli.Visible = false;

                //---------- récupération du classeur ---------
                classeur = appli.Workbooks.Open(System.Windows.Forms.Application.StartupPath + "/Facture.xlsx");
                

                //------ récupération la feuille ----------
                feuille = classeur.ActiveSheet;

                //1 = label, 2= qte, 3= prix
                //cell (ligne, colonne)

                for (int i = 0; i < 10 - conn.GetIdFacture(DateFacture, NameUser, conn.GetFirstNameCutomer(NameUser), PrixFacture).ToString().Count(); i++)
                {
                    IdFacture = "0" + IdFacture;
                }
                string[] date = DateFacture.Split('.');

                DateTime dateTime = new DateTime(Convert.ToInt32("20" + date[2]), Convert.ToInt32(date[1]), Convert.ToInt32(date[0]));

                feuille.Cells[2, 5] = dateTime.Day + " " + dateTime.ToString("MMM") + " " + dateTime.Year;
                feuille.Cells[3, 5] = IdFacture;
                feuille.Cells[12, 5] = TVA;

                //Nom et adresse du client
                string City = "";
                string Address = "";
                if (InfoCustomer[2] != "" && InfoCustomer[3] != "") { City = InfoCustomer[2] + ", " + InfoCustomer[3]; }
                if (InfoCustomer[0] != "" && InfoCustomer[1] != "") { Address = InfoCustomer[0] + " " + InfoCustomer[1]; }
                feuille.Cells[4, 5] = NameUser + " " + conn.GetFirstNameCutomer(NameUser);
                feuille.Cells[5, 5] = InfoCustomer[0] + " " + InfoCustomer[1]; //Address
                feuille.Cells[6, 5] = City; //Ville
                feuille.Cells[7, 5] = InfoCustomer[6]; //Téléphone

                //ajouter l'insertion des produits lier à la facture.
                //Insertion des produits lier à la facture
                int Line = 0;
                int x = 0;
                List<int> RowToDelete = new List<int>();
                for (int i = 0; i < InfoFacture.Count(); i += 3)
                {
                    if(i > 0)
                    {
                        //insertion de ligne dans excel.
                        Range lineExcel = (Range)feuille.Rows[11+x];
                        lineExcel.Insert();
                        x++;
                    }
                    feuille.Cells[10 + Line, 2] = InfoFacture[0 + i];
                    feuille.Cells[10 + Line, 3] = Convert.ToInt32(InfoFacture[1 + i]);      // quantité
                    feuille.Cells[10 + Line, 4] = Convert.ToDouble(InfoFacture[2 + i]);      //Prix
                    RowToDelete.Add(Line);
                    Line++;
                }

                //----- Auto-Enregistrement ---------------
                classeur.Close(true, M, M);

                SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

                var workbook = ExcelFile.Load("Facture.xlsx");
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.Description = "Sélectionnez le dossier où vous allez mettre la facture.";
                folderBrowserDialog.ShowDialog();
                workbook.Save(folderBrowserDialog.SelectedPath + "/Facture" + IdFacture + "_" + NameUser + "_" + conn.GetFirstNameCutomer(NameUser) + "_" + DateFacture + ".pdf");

                if (Enable)
                {
                    conn.ExportFacture(NameUser, conn.GetFirstNameCutomer(NameUser), Convert.ToDouble(Words[1]));
                }

                //---------- récupération du classeur ---------
                classeur = appli.Workbooks.Open(System.Windows.Forms.Application.StartupPath + "/Facture.xlsx");

                //------ récupération la feuille ----------
                feuille = classeur.ActiveSheet;

                //1 = label, 2= qte, 3= prix
                //cell (ligne, colonne)

                //Nom et adresse du client
                feuille.Cells[4, 5] = "";
                feuille.Cells[5, 5] = "";
                feuille.Cells[6, 5] = "";
                feuille.Cells[7, 5] = "";
                //Vider les articles de la facture
                feuille.Cells[10, 2] = "";
                feuille.Cells[10, 3] = "";
                feuille.Cells[10, 4] = "";

                //insertion de ligne dans excel.
                for (int i = 1; i < RowToDelete.Count(); i++)
                {
                    Range lineDelete = (Range)feuille.Rows[11];
                    lineDelete.Delete();
                }

                //----- Auto-Enregistrement ---------------
                classeur.Close(true, M, M);

                RefreshListFacture();
            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        //shows invoices that have been imported 
        private void BtnShowExportFacture_Click(object sender, EventArgs e)
        {
            if (Enable)
            {
                conn = new ConnectionDB();
                List<String> Facture = conn.GetFacture();
                LstFactures.Items.Clear();

                BtnExportFacture.Text = "Rexporter la facture";
                BtnModFacture.Enabled = false;
                BtnSuppFacture.Enabled = false;
                BtnShowExportFacture.Text = "Afficher les factures non-exportée";
                Enable = false;
                
                for (int i = 0; i < Facture.Count(); i += 5)
                {
                    if (Facture[0 + i].Length == 6)
                    {
                        Facture[0 + i] += "  ";
                    }
                    if (Facture[1 + i].Length <= 5)
                    {
                        Facture[1 + i] += "  ";
                    }
                    if (Facture[3 + i] == "Oui")
                    {
                        LstFactures.Items.Add(Facture[0 + i] + "          " + Facture[1 + i] + "\t" + Facture[2 + i] + "\t" + Facture[3 + i]);
                    }
                }

            }
            else
            {
                BtnExportFacture.Text = "Exporter la facture";
                BtnModFacture.Enabled = true;
                BtnSuppFacture.Enabled = true;
                BtnShowExportFacture.Text = "Afficher les factures exportée";
                Enable = true;
                RefreshListFacture();
            }
        }

        //Refresh the graphics
        public void RefreshListFacture()
        {
            conn = new ConnectionDB();
            List<String> Facture = conn.GetFacture();
            LstFactures.Items.Clear();
            for (int i = 0; i<Facture.Count(); i+=5)
            {
                if (Facture[0 + i].Length == 6)
                {
                    Facture[0 + i] += "  ";
                }
                if (Facture[1 + i].Length <= 5)
                {
                    Facture[1 + i] += "   ";
                }
                if (Enable)
                {
                    if (Facture[3 + i] == "Non")
                    {
                        LstFactures.Items.Add(Facture[0 + i] + "          " + Facture[1 + i] + "\t" + Facture[2 + i] + "\t" + Facture[3 + i]);
                    }
                }
                else
                {
                    if (Facture[3 + i] == "Oui")
                    {
                        LstFactures.Items.Add(Facture[0 + i] + "          " + Facture[1 + i] + "\t" + Facture[2 + i] + "\t" + Facture[3 + i]);
                    }
                }
            }
        }
    }
}
