﻿using PizzaTime.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmPizzaTimeMenu : Form
    {
        private ConnectionDB conn;
        private double PrixTotal = 0;
        List<string> Label = new List<string>();
        List<int> Price = new List<int>();
        List<string> TypeProduct = new List<string>();
        List<int> Qte = new List<int>();
        List<string> InfoProduct = new List<string>();

        public FrmPizzaTimeMenu()
        {
            InitializeComponent();
            System.Drawing.Image image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + "/LogoPizzaTime.png");
            Logo.Image = image;
            LblTotalPrice.Text = "0 CHF";
            RefreshListProduit();
        }

        private void BtnGestClients_Click(object sender, EventArgs e)
        {
            FrmGestionClients frmGestionClients = new FrmGestionClients();
            frmGestionClients.ShowDialog();
        }

        private void BtnGestProduits_Click(object sender, EventArgs e)
        {
            FrmGestionProduits frmGestionProduits = new FrmGestionProduits();
            frmGestionProduits.ShowDialog();
            RefreshListProduit();
        }

        private void BtnGestFactures_Click(object sender, EventArgs e)
        {
            FrmGestionFactures frmGestionFactures = new FrmGestionFactures();
            frmGestionFactures.ShowDialog();

        }

        private void BtnCommande_Click(object sender, EventArgs e)
        {
            List<string> ProductCommand = new List<string>();
            foreach(string item in LstProduitSelected.Items) { ProductCommand.Add(item); }
            PrixTotal = Convert.ToDouble(LblTotalPrice.Text.Substring(0, LblTotalPrice.Text.Length - 4));

            FrmCommande frmCommande = new FrmCommande(ProductCommand, PrixTotal);
            frmCommande.ShowDialog();
            RefreshListProduit();
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {

            //à vérifier plus tard car il est possible de simplifier.
            //Vérifier si la quantité est suffisante avec de sélectionner le produit ou avant la commande à voir

            InfoProduct = conn.GetProduct();
            if (LstProduitToSelect.SelectedItem != null) {
                string[] LastWord;
                string[] FirstWordSelect = LstProduitToSelect.SelectedItem.ToString().Split('\t');
                bool IsntExist = true;
                int IndexList = 0;
                int Quantity = 0;
                double Price = 0;

                for (int i = 0; i < LstProduitSelected.Items.Count; i++)
                {
                    string[] FirstWord = LstProduitSelected.Items[i].ToString().Split('\t');
                    if (FirstWord[0] == FirstWordSelect[0])
                    {
                        IsntExist = false;
                        IndexList = i;
                        Quantity = Convert.ToInt32(FirstWord[3]);
                        Price = Convert.ToDouble(FirstWord[1]);
                        break;
                    }
                }

                LastWord = LstProduitToSelect.SelectedItem.ToString().Split('\t');
                //Nom                        Prix          Type      Quantité
                // ajouter la vérification de quantité dans la modification de facture aussi.
                if (IsntExist)
                {
                    if (Convert.ToInt32(LastWord[3]) >= NbQuantitéSelected.Value)
                    {
                        LstProduitSelected.Items.Add(LastWord[0] + "\t" + LastWord[1] + "\t" + LastWord[2] + "\t" + NbQuantitéSelected.Value.ToString());
                        PrixTotal += Convert.ToDouble(LastWord[1]) * Convert.ToInt32(NbQuantitéSelected.Value);

                    }
                    else
                    {
                        MessageBox.Show("Il n'y a pas assez de ce produit pour l'ajouter à la commande");
                    }
                }
                else
                {

                    if (Convert.ToInt32(LastWord[3]) >= NbQuantitéSelected.Value + Quantity)
                    {
                        LstProduitSelected.Items[IndexList] = LastWord[0] + "\t" + LastWord[1] + "\t" + LastWord[2] + "\t" + (Quantity + NbQuantitéSelected.Value).ToString();
                        PrixTotal += Convert.ToDouble(LastWord[1]) * Convert.ToInt32(NbQuantitéSelected.Value);
                    }
                    else
                    {
                        MessageBox.Show("Il n'y a pas assez de ce produit pour l'ajouter à la commande");
                    }
                }

                LblTotalPrice.Text = PrixTotal + " CHF";
                NbQuantitéSelected.Value = 1;

            }
        }

        private void BtnUnselect_Click(object sender, EventArgs e)
        {
            
            if (LstProduitSelected.SelectedItem != null)
            {
                string[] LastWord;

                LastWord = LstProduitSelected.SelectedItem.ToString().Split('\t');
                PrixTotal -= Convert.ToDouble(LastWord[1]) * Convert.ToInt32(LastWord[3]); ;


                //string achat = LstProduitToSelect.SelectedItem.ToString();

                //achat = achat.Remove(achat.Length - 1);
                LblTotalPrice.Text = PrixTotal + " CHF";

                LstProduitSelected.Items.Remove(LstProduitSelected.SelectedItem);
            }
        }

        private void RefreshListProduit() {
            PrixTotal = 0;
            conn = new ConnectionDB();
            LstProduitToSelect.Items.Clear();
            LstProduitSelected.Items.Clear();
            LblTotalPrice.Text = "";

            InfoProduct = conn.GetProduct();
            for(int i = 0; i< InfoProduct.Count(); i+= 4)
            {
                LstProduitToSelect.Items.Add(InfoProduct[0 + i] + "\t" + InfoProduct[1 + i] + "\t" + InfoProduct[3 + i] + "\t" + InfoProduct[2 + i]);
            }
        }
    }
}
