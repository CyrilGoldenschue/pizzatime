﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime.Forms
{
    public partial class FrmCommande : Form
    {
        private ConnectionDB conn;
        List<string> ProductCommand;
        string products;
        int qte;
        double TotalPrice;
        public FrmCommande(List<string> productCommand, double totalPrice)
        {
            InitializeComponent();
            conn = new ConnectionDB();
            ProductCommand = productCommand;
            List<String> NameCustomer = new List<String>();

            NameCustomer = conn.GetCustomerName();
            
            TotalPrice = totalPrice;

            for(int i = 0; i<NameCustomer.Count(); i++)
            {
                CbxCustomer.Items.Add(NameCustomer[i]);
            }
        }

        private void RbtnCustomerNExist_CheckedChanged(object sender, EventArgs e)
        {
            CbxCustomer.Enabled = false;
            TxtAddressCustomer.Enabled = true;
            TxtCityCustomer.Enabled = true;
            TxtFirstnameCustomer.Enabled = true;
            TxtNameCustomer.Enabled = true;
            NbAddressCustomer.Enabled = true;
            NbNPA.Enabled = true;
            TxtEmailCustomer.Enabled = true;
            TxtPhoneCustomer.Enabled = true;
        }

        private void RbtnCustomerExist_CheckedChanged(object sender, EventArgs e)
        {
            CbxCustomer.Enabled = true;
            TxtAddressCustomer.Enabled = false;
            TxtCityCustomer.Enabled = false;
            TxtFirstnameCustomer.Enabled = false;
            TxtNameCustomer.Enabled = false;
            NbAddressCustomer.Enabled = false;
            NbNPA.Enabled = false;
            TxtEmailCustomer.Enabled = false;
            TxtPhoneCustomer.Enabled = false;
        }

        private void BtnValidationCommand_Click(object sender, EventArgs e)
        {
            string[] LastWord;
            conn = new ConnectionDB();

            FrmTVA frmTVA = new FrmTVA();
            frmTVA.ShowDialog();
            //The customer exist in the DB
            if (RbtnCustomerExist.Checked)
            {
                //You have choose a customer
                if (CbxCustomer.Text != "")
                {
                    
                    LastWord = CbxCustomer.Text.Split(' ');
                    int idFacture = conn.CreateFacture(LastWord[1], TotalPrice, frmTVA.TVA);

                    int x = 0;
                    while (x < ProductCommand.Count)
                    {

                        LastWord = ProductCommand[x].ToString().Split('\t');

                        products = LastWord[0].ToString();
                        qte = Convert.ToInt32(LastWord[3]);
                        double price = Convert.ToInt32(LastWord[3]) * Convert.ToDouble(LastWord[1]);

                        conn.CreateProductHasFactures(idFacture, products, qte, price, false);
                        conn.DeleteQtyProduct(conn.GetIdProduct(products), qte);
                        x++;
                    }
                    this.Close();
                }
                //You don't have choose a customer
                else
                {
                    MessageBox.Show("Vous n'avez pas choisi de client.");
                }
            }
            //The customer doesn't exist in the DB
            else
            {
                conn.CreateCustomer(TxtNameCustomer.Text, TxtFirstnameCustomer.Text, TxtAddressCustomer.Text, Convert.ToInt32(NbAddressCustomer.Value), TxtCityCustomer.Text, Convert.ToInt32(NbNPA.Value), TxtPhoneCustomer.Text, TxtEmailCustomer.Text);

                LastWord = CbxCustomer.Text.Split(' ');
                int idFacture = conn.CreateFacture(TxtNameCustomer.Text, TotalPrice, frmTVA.TVA);

                int x = 0;
                while (x < ProductCommand.Count)
                {

                    LastWord = ProductCommand[x].ToString().Split('\t');

                    products = LastWord[0].ToString();
                    qte = Convert.ToInt32(LastWord[3]);

                    conn.CreateProductHasFactures(idFacture, products, qte, TotalPrice, false);
                    conn.DeleteQtyProduct(conn.GetIdProduct(products), qte);
                    x++;
                }



                this.Close();
            }
        }
    }
}
