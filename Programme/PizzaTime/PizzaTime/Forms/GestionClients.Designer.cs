﻿namespace PizzaTime
{
    partial class FrmGestionClients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGestionClients));
            this.BtnAddClient = new System.Windows.Forms.Button();
            this.LstClients = new System.Windows.Forms.ListBox();
            this.BtnSuppClient = new System.Windows.Forms.Button();
            this.BtnModClient = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnAddClient
            // 
            this.BtnAddClient.Location = new System.Drawing.Point(16, 300);
            this.BtnAddClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddClient.Name = "BtnAddClient";
            this.BtnAddClient.Size = new System.Drawing.Size(159, 58);
            this.BtnAddClient.TabIndex = 0;
            this.BtnAddClient.Text = "Ajouter un client";
            this.BtnAddClient.UseVisualStyleBackColor = true;
            this.BtnAddClient.Click += new System.EventHandler(this.BtnAddClient_Click);
            // 
            // LstClients
            // 
            this.LstClients.FormattingEnabled = true;
            this.LstClients.ItemHeight = 16;
            this.LstClients.Location = new System.Drawing.Point(16, 31);
            this.LstClients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstClients.Name = "LstClients";
            this.LstClients.Size = new System.Drawing.Size(324, 260);
            this.LstClients.TabIndex = 4;
            // 
            // BtnSuppClient
            // 
            this.BtnSuppClient.Location = new System.Drawing.Point(183, 300);
            this.BtnSuppClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSuppClient.Name = "BtnSuppClient";
            this.BtnSuppClient.Size = new System.Drawing.Size(159, 58);
            this.BtnSuppClient.TabIndex = 5;
            this.BtnSuppClient.Text = "Supprimer un client";
            this.BtnSuppClient.UseVisualStyleBackColor = true;
            this.BtnSuppClient.Click += new System.EventHandler(this.BtnSuppClient_Click);
            // 
            // BtnModClient
            // 
            this.BtnModClient.Location = new System.Drawing.Point(16, 366);
            this.BtnModClient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnModClient.Name = "BtnModClient";
            this.BtnModClient.Size = new System.Drawing.Size(159, 58);
            this.BtnModClient.TabIndex = 7;
            this.BtnModClient.Text = "Modifier un client";
            this.BtnModClient.UseVisualStyleBackColor = true;
            this.BtnModClient.Click += new System.EventHandler(this.BtnModClient_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Nom                        Prénom";
            // 
            // FrmGestionClients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 425);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnModClient);
            this.Controls.Add(this.BtnSuppClient);
            this.Controls.Add(this.LstClients);
            this.Controls.Add(this.BtnAddClient);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(381, 472);
            this.MinimumSize = new System.Drawing.Size(381, 472);
            this.Name = "FrmGestionClients";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GestionClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAddClient;
        private System.Windows.Forms.ListBox LstClients;
        private System.Windows.Forms.Button BtnSuppClient;
        private System.Windows.Forms.Button BtnModClient;
        private System.Windows.Forms.Label label1;
    }
}