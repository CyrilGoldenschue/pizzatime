﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PizzaTime
{
    public partial class FrmGestionProduits : Form
    {
        ConnectionDB conn;

        public FrmGestionProduits()
        {
            InitializeComponent();
            RefreshListProduct();
        }

        private void RefreshListProduct()
        {
            conn = new ConnectionDB();
            List<String> Products = conn.GetProduct();
            try
            {
                LstProduits.Items.Clear();
                int i = 0;

                while (Products[i] != "")
                {
                    LstProduits.Items.Add(Products[0+i] + "\t" + Products[1 + i] + "\t" + Products[3 + i] + "\t" + Products[2 + i]);
                    i += 4;
                }
            }
            catch { }
        }

        private void BtnAddQte_Click(object sender, EventArgs e)
        {
            if (LstProduits.SelectedIndex >= 0)
            {
                string[] LastWord;
                LastWord = LstProduits.SelectedItem.ToString().Split('\t');

                FrmAdd_ModQte frmAdd_ModQte = new FrmAdd_ModQte(LastWord[0]);
                frmAdd_ModQte.ShowDialog();
                RefreshListProduct();
            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        private void BtnAddProduit_Click(object sender, EventArgs e)
        {
            FrmAdd_ModProduit frmAdd_ModProduit = new FrmAdd_ModProduit(false);
            frmAdd_ModProduit.ShowDialog();
            RefreshListProduct();
        }

        private void BtnModProduit_Click(object sender, EventArgs e)
        {
            if (LstProduits.SelectedIndex >= 0)
            {
                conn = new ConnectionDB();
                string[] LastWord;
                List<string> Info = new List<string>();

                LastWord = LstProduits.SelectedItem.ToString().Split('\t');

                FrmAdd_ModProduit frmAdd_ModProduit = new FrmAdd_ModProduit(true, LastWord[0], LastWord[2], Convert.ToInt32(LastWord[1]), Convert.ToInt32(LastWord[3]));
                frmAdd_ModProduit.ShowDialog();

                RefreshListProduct();

            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }

        private void BtnSuppProduit_Click(object sender, EventArgs e)
        {
            if (LstProduits.SelectedIndex >= 0)
            {
                conn = new ConnectionDB();
                string[] LastWord;

                LastWord = LstProduits.SelectedItem.ToString().Split('\t');
                conn.DeleteProduct(LastWord[0]);

                RefreshListProduct();
            }
            else
            {
                MessageBox.Show("Vous n'avez rien sélectionné avant d'appuyer sur le bouton de modification.");
            }
        }
    }
}
