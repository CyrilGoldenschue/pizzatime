﻿namespace PizzaTime
{
    partial class FrmGestionFactures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGestionFactures));
            this.BtnModFacture = new System.Windows.Forms.Button();
            this.BtnSuppFacture = new System.Windows.Forms.Button();
            this.LstFactures = new System.Windows.Forms.ListBox();
            this.BtnExportFacture = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnShowExportFacture = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnModFacture
            // 
            this.BtnModFacture.Location = new System.Drawing.Point(16, 300);
            this.BtnModFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnModFacture.Name = "BtnModFacture";
            this.BtnModFacture.Size = new System.Drawing.Size(159, 58);
            this.BtnModFacture.TabIndex = 11;
            this.BtnModFacture.Text = "Modifier une facture";
            this.BtnModFacture.UseVisualStyleBackColor = true;
            this.BtnModFacture.Click += new System.EventHandler(this.BtnModFacture_Click);
            // 
            // BtnSuppFacture
            // 
            this.BtnSuppFacture.Location = new System.Drawing.Point(183, 300);
            this.BtnSuppFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnSuppFacture.Name = "BtnSuppFacture";
            this.BtnSuppFacture.Size = new System.Drawing.Size(159, 58);
            this.BtnSuppFacture.TabIndex = 10;
            this.BtnSuppFacture.Text = "Supprimer une facture\r\n";
            this.BtnSuppFacture.UseVisualStyleBackColor = true;
            this.BtnSuppFacture.Click += new System.EventHandler(this.BtnSuppFacture_Click);
            // 
            // LstFactures
            // 
            this.LstFactures.FormattingEnabled = true;
            this.LstFactures.ItemHeight = 16;
            this.LstFactures.Location = new System.Drawing.Point(16, 31);
            this.LstFactures.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstFactures.Name = "LstFactures";
            this.LstFactures.Size = new System.Drawing.Size(324, 260);
            this.LstFactures.TabIndex = 9;
            // 
            // BtnExportFacture
            // 
            this.BtnExportFacture.Location = new System.Drawing.Point(16, 366);
            this.BtnExportFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnExportFacture.Name = "BtnExportFacture";
            this.BtnExportFacture.Size = new System.Drawing.Size(159, 58);
            this.BtnExportFacture.TabIndex = 12;
            this.BtnExportFacture.Text = "Exporter une facture";
            this.BtnExportFacture.UseVisualStyleBackColor = true;
            this.BtnExportFacture.Click += new System.EventHandler(this.BtnExportFacture_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(324, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "Date                 Client              Prix            Exportée\r\n";
            // 
            // BtnShowExportFacture
            // 
            this.BtnShowExportFacture.Location = new System.Drawing.Point(183, 366);
            this.BtnShowExportFacture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnShowExportFacture.Name = "BtnShowExportFacture";
            this.BtnShowExportFacture.Size = new System.Drawing.Size(159, 58);
            this.BtnShowExportFacture.TabIndex = 16;
            this.BtnShowExportFacture.Text = "Afficher les factures exportée\r\n";
            this.BtnShowExportFacture.UseVisualStyleBackColor = true;
            this.BtnShowExportFacture.Click += new System.EventHandler(this.BtnShowExportFacture_Click);
            // 
            // FrmGestionFactures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 426);
            this.Controls.Add(this.BtnShowExportFacture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnExportFacture);
            this.Controls.Add(this.BtnModFacture);
            this.Controls.Add(this.BtnSuppFacture);
            this.Controls.Add(this.LstFactures);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(377, 473);
            this.MinimumSize = new System.Drawing.Size(377, 473);
            this.Name = "FrmGestionFactures";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GestionFactures";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnModFacture;
        private System.Windows.Forms.Button BtnSuppFacture;
        private System.Windows.Forms.ListBox LstFactures;
        private System.Windows.Forms.Button BtnExportFacture;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnShowExportFacture;
    }
}