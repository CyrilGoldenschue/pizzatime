﻿namespace PizzaTime
{
    partial class FrmAdd_ModQte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAdd_ModQte));
            this.LblQteProduit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NbQty = new System.Windows.Forms.NumericUpDown();
            this.BtnAddQte = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NbQty)).BeginInit();
            this.SuspendLayout();
            // 
            // LblQteProduit
            // 
            this.LblQteProduit.AutoSize = true;
            this.LblQteProduit.Location = new System.Drawing.Point(17, 16);
            this.LblQteProduit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblQteProduit.Name = "LblQteProduit";
            this.LblQteProduit.Size = new System.Drawing.Size(0, 17);
            this.LblQteProduit.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quantité";
            // 
            // NbQty
            // 
            this.NbQty.Location = new System.Drawing.Point(88, 57);
            this.NbQty.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NbQty.Name = "NbQty";
            this.NbQty.Size = new System.Drawing.Size(103, 22);
            this.NbQty.TabIndex = 2;
            // 
            // BtnAddQte
            // 
            this.BtnAddQte.Location = new System.Drawing.Point(217, 57);
            this.BtnAddQte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnAddQte.Name = "BtnAddQte";
            this.BtnAddQte.Size = new System.Drawing.Size(252, 28);
            this.BtnAddQte.TabIndex = 3;
            this.BtnAddQte.Text = "Ajouter";
            this.BtnAddQte.UseVisualStyleBackColor = true;
            this.BtnAddQte.Click += new System.EventHandler(this.BtnAddQte_Click);
            // 
            // FrmAdd_ModQte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 91);
            this.Controls.Add(this.BtnAddQte);
            this.Controls.Add(this.NbQty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblQteProduit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(501, 138);
            this.MinimumSize = new System.Drawing.Size(501, 138);
            this.Name = "FrmAdd_ModQte";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ajout/Modification quantité";
            ((System.ComponentModel.ISupportInitialize)(this.NbQty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblQteProduit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NbQty;
        private System.Windows.Forms.Button BtnAddQte;
    }
}